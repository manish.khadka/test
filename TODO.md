# TODO list

### Implementation and Improvements

- [ ] sass/scss implementation using variables and partial imports
  - [ ] POC first maybe in one of the help sites
  - [ ] migrate from bootstrap 4.x.x to 5.x.x
        https://getbootstrap.com/docs/5.0/migration/
- [ ] image sharp default config
- [ ] packages version update(non breaking)
  - [ ] update eslint-plugin-react-hooks to support latest eslint
- [ ] Optimize graphql queries
- [ ] CMS code portability
  - [x] move custom files into a new theme
  - [x] refactor code
  - [x] support for ENV constants
  - [ ] .htaccess and .htpasswords
  - [ ] wp-config
- [ ] mock migrate CMS using all in one
- [ ] replace iframe implementation in training mst
- [ ] add a precommit lint - husky (for smoother colaboration)

### Packages to remove

- [ ] gatsby-plugin-svgr (svg optimization and using it as react components)
- [ ] gatsby-plugin-netlify (pass security headers)
- [ ] gatsby-transformer-remark (parse markdown files)
- [ ] gatsby-transformer-yaml-full (parse yaml)
- [ ] gatsby-yaml-full-markdown (parse yaml with markdown tags)
- [ ] gatsby-source-filesystem (sourcing data from your local filesystem)
- [ ] basically all remark plugins since we wont be using marksown files
  - [ ] gatsby-remark-copy-linked-files (copies files linked in md)
  - [ ] gatsby-remark-embed-video (embed vide using link in md)
  - [ ] gatsby-remark-images (process img in md)
  - [ ] gatsby-remark-prismjs (syntax highlighting)
  - [ ] gatsby-remark-responsive-iframe (resp iframe wrapper)
  - [ ] gatsby-remark-smartypants (replaces punctuations in md)

### Utilize or replace

- [ ] gatsby-plugin-offline (add offline support?)
- [ ] formik (replace with custom input/form?)
