# ApWebsites

This project is the monorepo for all **AP websites** applications.

## Motivation

To share components and utilities among multiple related repositories.

## Apps

| Sites                         | PROJECTNAME       | DEVURL                                    | URL                                   |
| ----------------------------- | ----------------- | ----------------------------------------- | ------------------------------------- |
| Advanced Pedagogy Site        | ap-site           | pending                                   | https://www.advancedpedagogy.com/     |
| Jelajahilmu Help Site         | jeli-help-site    | https://mono-jeli-help.innovatetech.io/   | https://help.jelajahilmu.com/         |
| Jelajahilmu Site              | jeli-site         | https://mono-jeli.innovatetech.io/        | https://jelajahilmu.com/              |
| mySecondTeacher Help Site     | mst-help-site     | https://mono-mst-help.innovatetech.io/    | https://help.mysecondteacher.com/     |
| mySecondTeacher Nepal Site    | mst-nepal-site    | https://mono-mst-nepal.innovatetech.io/   | https://mysecondteacher.com.np/       |
| mySecondTeacher Training Site | mst-training-site | https://mstnepaltraining.innovatetech.io/ | https://training.mysecondteacher.com/ |
| Nepal Help Site               | nepal-help-site   | pending                                   | pending                               |

### Shared-UI

- Common components and styles

### Utils

- Common utilities for the applications.

## Installation

1. Clone the repository (with SSH, recommended) at your desired local directory
   ```sh
   git clone git@gitlab.com:innovate-tech/ap-websites/ap-websites-monorepo.git
   ```
1. Install the dependencies and devDependencies
   ```sh
   npm install
   ```
1. Start the server.
   ```sh
   npm start <PROJECTNAME>
   ```

## NPM Scripts

| Command                            | Description                                                                                 |
| ---------------------------------- | ------------------------------------------------------------------------------------------- |
| `npm i`                            | Installs `npm` dependencies as well as builds vendor dependencies                           |
| `npm start <PROJECTNAME>`          | Starts an application in development mode list of projects can be found in `workspace.json` |
| `nx serve <PROJECTNAME>`           | Same as `npm start <PROJECTNAME>`                                                           |
| `npm run format`                   | Runs prettier to format the code. `Always run this before every commit`                     |
| `npm run build <PROJECTNAME>`      | Builds production version of a project into `public` folder                                 |
| `nx build <PROJECTNAME>`           | Same as `npm run build <PROJECTNAME>`                                                       |
| `npm run serve:prod <PROJECTNAME>` | Serves the production builds of a project                                                   |
| `nx serve <PROJECTNAME> --prod`    | Same as `npm run serve:prod <PROJECTNAME>`                                                  |

> You can refer to the **scripts** section of the `package.json` file for more
> commands. Other workspace related commands are explaned below.

> Use `affected` to find out the projects affected by the change.

> List of projects can be found in `workspace.json`

## Development server

Run `npm run <PROJECTNAME>` or `nx serve <PROJECTNAME>` (if you have installed
nx-cli globally ) for a dev server. Navigate to http://localhost:4200/. The app
will automatically reload if you change any of the source files.

## What's inside?

A quick look at the top-level files and directories you'll see in a this
project.

    .
    ├── 📂 apps
    │   ├── 📂 ap-site
    │   ├── 📂 jeli-help-site
    │   ├── 📂 jeli-site
    │   ├── 📂 mst-help-site
    │   │   ├── 📂 src
    │   │   ├── 📂 components
    │   │   ├── 📂 images
    │   │   ├── 📂 pages
    │   │   ├── 📂 templates
    │   │   ├── 📂 static
    │   │   ├── 📜 .babelrc
    │   │   ├── 📜 .eslintrc.json
    │   │   ├── 📜 .gitlab-ci.yml
    │   │   ├── 📜 gatsby-browser.js
    │   │   ├── 📜 gatsby-config.js
    │   │   ├── 📜 gatsby-node.js
    │   │   ├── 📜 gatsby-ssr.js
    │   │   ├── 📜 global.d.ts
    │   │   ├── 📜 package.json
    │   │   ├── 📜 project.json
    │   │   ├── 📜 README.md
    │   │   ├── 📜 tsconfig.json
    │   │   └── 📜 tsconfig.app.json
    │   ├── 📂 mst-nepal-site
    │   ├── 📂 mst-training-site
    │   └── 📂 nepal-help-site
    ├── 📂 libs
    ├── 📂 node_modules
    ├── 📂 tools
    ├── 📜 README.md
    ├── 📜 .env.example
    ├── 📜 .eslintrc.json
    ├── 📜 .gitignore
    ├── 📜 .gitlab-ci.yml
    ├── 📜 .prettierignore
    ├── 📜 .prettierrc
    ├── 📜 babel.config.json
    ├── 📜 nx.json
    ├── 📜 package.json
    ├── 📜 tsconfig.base.json
    └── 📜 workspace.json

1.  **`/node_modules`**: This directory contains all of the modules of code that
    your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you
    will see on the front-end of your site (what you see in the browser) such as
    your site header or a page template. `src` is a convention for “source
    code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not
    maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for
    [Prettier](https://prettier.io/). Prettier is a tool to help keep the
    formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage
    of the
    [Gatsby browser APIs](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-browser/)
    (if any). These allow customization/extension of default Gatsby settings
    affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby
    site. This is where you can specify information about your site (metadata)
    like the site title and description, which Gatsby plugins you’d like to
    include, etc. (Check out the
    [config docs](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-config/)
    for more detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of
    the
    [Gatsby Node APIs](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/)
    (if any). These allow customization/extension of default Gatsby settings
    affecting pieces of the site build process.

8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of
    the
    [Gatsby server-side rendering APIs](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-ssr/)
    (if any). These allow customization of default Gatsby settings affecting
    server-side rendering.

9.  **`LICENSE`**: This Gatsby starter is licensed under the 0BSD license. This
    means that you can see this file as a placeholder and replace it with your
    own license.

10. **`package-lock.json`** (See `package.json` below, first). This is an
    automatically generated file based on the exact versions of your npm
    dependencies that were installed for your project. **(You won’t change this
    file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes
    things like metadata (the project’s name, author, etc). This manifest is how
    npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about
    your project.

## Understand your workspace

Run `npm run graph` or `nx graph` to see a diagram of the dependencies of your
projects.

# Resource links

[gatsby] https://www.gatsbyjs.com/

[wordpress] https://wordpress.org/

[graphql] https://graphql.org/

[nx] https://nx.dev

[reactjs] https://reactjs.org/

[thunk] https://redux-toolkit.js.org/

[typescript] https://www.typescriptlang.org/

[css modules with scss]
https://create-react-app.dev/docs/adding-a-css-modules-stylesheet/

[twitter bootstrap] https://getbootstrap.com/

[eslint] https://eslint.org/

[prettier] https://prettier.io/

[node js] https://nodejs.org

[npm] https://www.npmjs.com/

[gitlab flow]
https://docs.google.com/document/d/1N-N5TTvOfVZ_EGW2ak9uAHC-v-8sbioMmVTgv-NlXa8/edit

[⬆ Move to Top](#ApWebsites)
