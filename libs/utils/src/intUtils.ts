export const isEven = (n: int) => {
  return n % 2 === 0
}

export const isOdd = (n: int) => {
  return Math.abs(n % 2) === 1
}
