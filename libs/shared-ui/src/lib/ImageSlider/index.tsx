import React from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './index.scss'
import DynamicImage from '../DynamicImage'

/* eslint-disable-next-line */
export interface ImageSliderProps {
  imgList: Array<string>
}

export function ImageSlider(props: ImageSliderProps) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: false,
        },
      },
    ],
  }
  return (
    <Slider
      className="parent__slides adaptiveHeight slider slick-initialized slick-slider slick-dotted"
      {...settings}
    >
      {props.imgList.map((result, index) => (
        // <img src={result} className="d-block w-100" alt="" key={index} />
        <React.Fragment key={result.publicURL}>
          <DynamicImage
            imageData={result}
            className="d-block w-100"
            alt=""
            loading="lazy"
          />
        </React.Fragment>
      ))}
    </Slider>
  )
}

export default ImageSlider
