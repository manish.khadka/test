import React from 'react'
/* eslint-disable-next-line */
export interface FooterProps {}

export function Footer(props: FooterProps) {
  return (
    <footer className="text-white bottom bottom-content mx-auto">
      © mySecondTeacher {new Date().getFullYear()}
    </footer>
  )
}

export default Footer

// export const Footer = () => {
//   return (
//     <footer className="text-white bottom bottom-content mx-auto">
//       © mySecondTeacher {new Date().getFullYear()}
//     </footer>
//   )
// }

// export default Footer
