import { GatsbyImage } from 'gatsby-plugin-image'
import React from 'react'
import DynamicImage from '../DynamicImage'

/* eslint-disable-next-line */
export interface InteractiveArtworkProps {
  image:
    | {
        publicURL: string
        childImageSharp: object
      }
    | string
}

export function InteractiveArtwork(props: InteractiveArtworkProps) {
  return (
    <div className="interactive-container">
      <div className="interactive interactive--image-interactive ">
        {/* <img
          src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/panorama_optimized.jpg"
          className="interactive__image interactive__image--1"
          alt=""
        /> */}
        <DynamicImage
          imageData={props.image}
          alt={''}
          className="interactive__image interactive__image--1"
        />
        <div className="interactive__interactions">
          <span
            data-index="1"
            className="interactive__interaction-item"
            style={{ left: '16.423%', top: '72.001%' }}
          />
          <span
            data-index="3"
            className="interactive__interaction-item"
            style={{ left: '60.262%', top: '74.189%' }}
          />
          <span
            data-index="4"
            className="interactive__interaction-item"
            style={{ left: '44.404%', top: '37.544%' }}
          />
          <span
            data-index="5"
            className="interactive__interaction-item"
            style={{ left: '57.397%', top: '35.361%' }}
          />
          <span
            data-index="6"
            className="interactive__interaction-item"
            style={{ left: '52.322%', top: '27.373%' }}
          />
          <span
            data-index="7"
            className="interactive__interaction-item"
            style={{ left: '13.932%', top: ' 18.96%' }}
          />

          <span
            data-index="8"
            className="interactive__interaction-item"
            style={{ left: '62.36%', top: ' 40.84%' }}
          />
          <span
            data-index="9"
            className="interactive__interaction-item"
            style={{ left: '75.056%', top: '12.734%' }}
          />
        </div>
        <div className="interactive__tooltip-image"></div>
      </div>
    </div>
  )
}

export default InteractiveArtwork
