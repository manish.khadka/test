import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import React from 'react'
/* eslint-disable-next-line */
export interface DynamicImageProps {
  imageData:
    | {
        publicURL: string
        childImageSharp: object
      }
    | string
  alt: string
  imageStyle?: object
  className?: string
  loading?: 'lazy' | 'eager' | undefined
}

export function DynamicImage(props: DynamicImageProps) {
  // if (typeof(props.imageData) == 'object') {
  if (props.imageData.childImageSharp) {
    return (
      <GatsbyImage
        image={getImage(props.imageData)}
        style={props.imageStyle}
        alt={props.alt}
        className={props.className}
        loading={props.loading}
      />
    )
  } else if (props.imageData) {
    return (
      <img
        style={props.imageStyle}
        src={props.imageData?.publicURL || props.imageData}
        alt={props.alt}
        className={props.className}
      />
    )
  } else {
    return null
  }
}

export default DynamicImage
