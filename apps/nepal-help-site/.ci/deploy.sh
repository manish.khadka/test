#!/usr/bin/env bash

set -euo pipefail

# upload maximum file as possble at time
aws configure set s3.max_concurrent_requests 300

# export variables taken from build stage
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID_
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY_

# start uploading to s3 bucket
aws s3 sync apps/nepal-help-site/public s3://$S3_BUCKET/$NEPAL_HELP --only-show-errors --delete --exclude "*.js.map"

# start invalidating cache for index html
aws cloudfront create-invalidation --distribution-id $DISTRIBUTION_ID --paths "/$NEPAL_HELP/*"
