// import './card.module.scss';
import React from 'react'

const Card = ({ background, list, heading, icon }) => {
  const getBackgrourdColor = (background: string) => {
    switch (background) {
      case 'gray':
        return '#f9f7ff'
      case 'green':
        return '#ebf9f6'
      case 'blue':
        return '#e5f1ff'
      default:
        return '#f9f7ff'
    }
  }
  return (
    <div
      className="card mb-4 card--title"
      style={{ backgroundColor: getBackgrourdColor(background) }}
    >
      <div className="card-body">
        <div className="d-flex align-items-center">
          <img
            src={icon.publicURL}
            height={48}
            width={48}
            alt={heading}
            style={{ marginRight: '1rem' }}
          />
          <h4 className="heading h6">{heading}</h4>
        </div>
        <div className="mt-2">
          <ul className="arrow__li-icon">
            {list.map((item) => (
              <li key={item.listItem}>{item.listItem}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}
export default Card
