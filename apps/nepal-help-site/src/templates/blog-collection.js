import React from 'react'
import { graphql } from 'gatsby'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import AccordionPanel from 'components/AccordionPanel'
import { uglifyString } from '@ap-websites/utils'

const BlogCollectionTemplate = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const page = data.wpHelpContentCategory
  const posts = data.allWpNepalHelpSiteContent.nodes

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title={page.name} description={`FAQ regarding ${page.name}`} />
      <header>
        <h1 className="heading" itemProp="headline">
          {page.name}
        </h1>
      </header>
      <div className="accordion panel-group" id="accordion">
        {posts.map((result) => (
          <AccordionPanel
            key={result.id}
            id={uglifyString(result.title)}
            title={result.title}
            content={result.content}
            embedVideoId={result.helpContent.youtubeVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogCollectionTemplate

export const pageQuery = graphql`
  query BlogCollectionBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    wpHelpContentCategory(slug: { regex: $slug }) {
      name
    }
    allWpNepalHelpSiteContent(
      sort: { fields: date, order: ASC }
      filter: {
        nepalHelpContentCategories: {
          nodes: { elemMatch: { slug: { regex: $slug } } }
        }
      }
    ) {
      nodes {
        id
        title
        nepalHelpContentCategories {
          nodes {
            name
          }
        }
        slug
        helpContent {
          youtubeVideoId
        }
        content
      }
    }
  }
`
