import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import EmbedVideo from 'components/EmbedVideo'

const BlogPostTemplate = ({ data, location }) => {
  const post = data.wpNepalHelpSiteContent
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const { previous, next } = data

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title={post.title} description={post.excerpt} />
      <article
        className="blog-post"
        itemScope
        itemType="http://schema.org/Article"
      >
        <Link
          className="badge badge-primary p-1 px-2 my-3 rounded-pill"
          to={'/' + post.nepalHelpContentCategories.nodes[0].slug + '/'}
        >
          {post.nepalHelpContentCategories.nodes[0].name}
        </Link>
        <header>
          <h1 className="heading mb-4" itemProp="headline">
            {post.title}
          </h1>
        </header>
        <section
          dangerouslySetInnerHTML={{ __html: post.content }}
          itemProp="articleBody"
        />
        {post.helpContent.youtubeVideoId && (
          <EmbedVideo
            title={post.title}
            embedVideoId={post.helpContent.youtubeVideoId}
            fullWidth={false}
          />
        )}
        <p>
          If you have additional queries, then feel free to contact our support
          team via our Online Chat feature. Our support team are available 24/7
          for you. You can access the online chat even without logging in.
        </p>
        <hr />
      </article>
      <nav className="blog-post-nav">
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <Link to={`/${previous.slug}`} rel="prev">
                ← {previous.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={`/${next.slug}`} rel="next">
                {next.title} →
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug(
    $id: String!
    $previousPostId: String
    $nextPostId: String
  ) {
    site {
      siteMetadata {
        title
      }
    }
    wpNepalHelpSiteContent(id: { eq: $id }) {
      id
      title
      content
      helpContent {
        youtubeVideoId
      }
      nepalHelpContentCategories {
        nodes {
          name
          slug
        }
      }
      slug
      excerpt
    }
    previous: wpNepalHelpSiteContent(id: { eq: $previousPostId }) {
      slug

      title
    }
    next: wpNepalHelpSiteContent(id: { eq: $nextPostId }) {
      slug

      title
    }
  }
`
