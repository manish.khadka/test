import React from 'react'
import { graphql, Link } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'

const NotFoundPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title="404: Not Found" />
      <section class="section pt-5 section--bg-light">
        <div class="container">
          <div class="row justify-content-start">
            <div class="col-lg-10 py-0">
              <div class="heading text-left h3 mb-3">
                Oops! The page you were looking for doesn't exist.
              </div>
              <p className="text-left">
                Looks like you got lost. Click <Link to="/">here</Link> to go to
                home page.
              </p>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
