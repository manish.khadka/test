/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  // Define a template for blog post
  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const blogCollection = path.resolve(`./src/templates/blog-collection.js`)

  // Get all markdown blog posts sorted by date
  const result = await graphql(
    `
      {
        allWpHelpContentCategory(sort: { fields: id, order: ASC }) {
          nodes {
            name
            id
            slug
          }
        }
        allWpNepalHelpSiteContent(
          sort: { fields: date, order: ASC }
          limit: 1000
        ) {
          nodes {
            id
            title
            nepalHelpContentCategories {
              nodes {
                name
                slug
              }
            }
            slug
          }
        }
      }
    `,
  )

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your blog posts`,
      result.errors,
    )
    return
  }

  const posts = result.data.allWpNepalHelpSiteContent.nodes
  const categories = result.data.allWpHelpContentCategory.nodes

  // Create blog posts pages
  // But only if there's at least one markdown file found at "content/blog" (defined in gatsby-config.js)
  // `context` is available in the template as a prop and as a variable in GraphQL

  if (posts.length > 0) {
    posts.forEach((post, index) => {
      const previousPostId = index === 0 ? null : posts[index - 1].id
      const nextPostId = index === posts.length - 1 ? null : posts[index + 1].id
      const postSlug = post.slug

      let template = blogPost
      // const categorySlug = post.nepalHelpContentCategories.nodes[0].slug
      // const title = post.title

      // if (title === category) {
      //   // eslint-disable-next-line @typescript-eslint/no-unused-vars
      //   template = blogCollection
      // }
      createPage({
        path: '/' + postSlug + '/',
        component: template,
        context: {
          id: post.id,
          slug: '/' + postSlug + '/',
          previousPostId,
          nextPostId,
        },
      })
    })
  }
  if (categories.length > 0) {
    categories.forEach((category, index) => {
      const categorySlug = category.slug
      let template = blogCollection

      createPage({
        path: '/' + categorySlug + '/',
        component: template,
        context: {
          id: category.id,
          slug: '/' + categorySlug + '/',
        },
      })
    })
  }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })

    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  // Explicitly define the siteMetadata {} object
  // This way those will always be defined even if removed from gatsby-config.js

  // Also explicitly define the Markdown frontmatter
  // This way the "MarkdownRemark" queries will return `null` even when no
  // blog posts are stored inside "content/blog" instead of returning an error
  createTypes(`
    type SiteSiteMetadata {
      author: Author
      siteUrl: String
      social: Social
    }

    type Author {
      name: String
      summary: String
    }

    type Social {
      twitter: String
    }

    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }

    type Frontmatter {
      title: String
      description: String
      date: Date @dateformat
    }

    type Fields {
      slug: String
    }
  `)
}
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        '@components': path.resolve(__dirname, 'src/components'),
        '@static': path.resolve(__dirname, 'static'),
      },
    },
  })
}

// for the third-party modules/libraries/npms depending on the window object
// exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
//   if (stage === "build-html") {
//     actions.setWebpackConfig({
//       module: {
//         rules: [
//           {
//             test: /offending-module/,
//             use: loaders.null(),
//           },
//         ],
//       },
//     })
//   }
// }
