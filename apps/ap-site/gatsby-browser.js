/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
// import '@ap-websites/typeface-foco'
import './src/normalize.css'
// custom CSS styles
import './src/style.scss'
