page: privacy-policy
pageTitle: Privacy Policy
pageData: !markdown |
  **Advanced Pedagogy**

  Cookies Policy

  Last Update: 19th June 2019

  This Cookies Policy explains what cookies are, how we use cookies, how third parties we may partner with may use cookies, your choices regarding cookies and further information about cookies.

  **Who are we?**

  Advanced Pedagogy is an edtech company that is focused on the international education sector. We create thousands of minutes of interactive video-based content that teaches entire subject syllabuses, provide 24×7 online tutors, and produce individualised diagnostic reports to help students with revisions and teachers with interventions.

  Registration Details:

  Registration No -- 201800596E

  Company Name -- Advanced Pedagogy PTE. LTD.

  Company Address -- North Bridge Road, #08-08, High Street Center, Singapore 179094

  School leaders work with us to lower teaching burdens and improve their students' exam scores. Adpeda (Short for Advanced Pedagogy) utilises design, development and multimedia services from verticals/companies under Innovate Tech, enabling in house production resulting in Impeccable Product Packaging. Adpeda comprises of three products which enable us to deliver the services mentioned above to our customers.

  1.  [My Second Teacher](http://mysecondteacher.com/)
  2.  Apollo
  3.  IVy

  This policy explains how we process information that can be used to directly or indirectly identify an individual ("personal data"): thus, this policy applies to the situations in which we act as data controller of personal data and explains when and why we collect personal information about individuals, how we use it, the conditions under which we may disclose it to others and how we keep it secure.

  **Who is our Data Protection Officer?**

  In case you have any questions regarding this policy, you can contact our Data Protection Officer. Please send your enquiries to the person responsible below. Thus, in case you have any questions regarding this policy and/or our privacy practices: support@advancedpedagogy.com

  [support@advancedpedagogy.com](mailto:support@mysecondteacher.com)

  Data Protection Representative: Benson Soong

  Address: Advanced Pedagogy PTE. LTD, North Bridge Road, #08-08, High Street Center, Singapore 179094

  **What are cookies?**

  Cookies are small pieces of text sent to your web browser by a website you visit. A cookie file is stored in your device and allows us or a third-party to recognise you and make your next visit easier and the service more useful to you: thus, it enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period of time so you don't have to keep re-entering them whenever you come back to the site or browse from one page to another.

  Cookies can be "persistent" or "session" cookies. *Persistent* cookies remain on your personal computer or mobile device when you go offline, while *session* cookies are deleted as soon as you close your web browser.

  **How does Advanced Pedagogy use cookies?**

  When you use and access our services, we may place a number of cookies files in your web browser. We use cookies for the following purposes:

  -   to enable certain functions of the services;
  -   to provide analytics;
  -   to store your preferences;

  We use both session and persistent cookies on the website and we use different types of cookies to run it:

  -   **Essential Cookies |** We may use essential cookies to authenticate users and prevent fraudulent use of user accounts;
  -   **Preferences Cookies |** We may use preferences cookies to remember information that changes the way the services behave or look, such as the "remember me" functionality of a registered user or a user's language preference;
  -   **Analytics Cookies |** We may use analytics cookies to track information about how the website is used so that we can make improvements. We may also use analytics cookies to  test new advertisements, pages, features or new functionality of the website to see how our users react to them;

  **First Party Cookies**

  We will set some cookies that are essential for our website to operate correctly. These cookies, none of which capture personally identifiable information, are:

  -   **Analytics Cookies | **We may use analytics cookies to track information how the website is used so that we can make improvements. We may also use analytics cookies to test new advertisements, pages, features or new functionality of the website to see how our users react to them;

  **Third Party Cookies**

  In addition to our own cookies, we may also use various third parties cookies to report usage statistics of the website, deliver advertisements on and through the website, and so on. Such cookies may include:

  -   Facebook Pixel -- As a Facebook advertiser we install the Facebook or Atlas pixel on our website in order to measure ad conversions or retarget advertisements on Facebook for our customers, to assist them with applications and information.
  -   Google Pixel -- Our partners will collect data and use cookies for ad personalization and measurement.

  **What are your choices regarding cookies?**

  You can control and/or delete cookies as you wish: thus, you can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you delete cookies or refuse to accept them, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.

  **Google Chrome**

  Please visit this page from Google:

  <https://support.google.com/accounts/answer/32050>

  **Internet Explorer**

  Please visit this page from Microsoft:

  <http://support.microsoft.com/kb/278835>

  **Mozilla Firefox**

  Please visit this page from Mozilla:

  <https://support.mozilla.org/en-US/kb/delete-cookies-remove-info-websitesstored>

  **Safari**

  Please visit this page from Apple:

  <https://support.apple.com/kb/PH21411?locale=en_US>

  For any other web browser, please visit your web browser's official web pages.

  **Where can you find more information about cookies?**

  You can learn more about cookies and the following third-party websites:

  **All About Cookies**

  <http://www.allaboutcookies.org/>

  **Network Advertising Initiative**

  <http://www.networkadvertising.org/>

  **What about the changes to this policy?**

  We keep this policy under regular review, so please check this page occasionally to ensure that you're happy with any changes. This policy was last updated on 19th June 2019

  * * * * *

  These are the cookies policy of https://www.advancedpedagogy.com
