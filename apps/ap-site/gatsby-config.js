module.exports = {
  siteMetadata: {
    title: `Advanced Pedagogy`,
    description: `Advanced Pedagogy is an edtech company that is focused on the international education sector. Advanced Pedagogy create thousands of minutes of interactive video-based content that teaches entire subject syllabuses.`,
    siteUrl: `https://www.advancedpedagogy.com/`,
    social: {
      twitter: `adpedagogy`,
      facebook: `https://www.facebook.com/advancedpedagogy/`,
      instagram: `https://www.instagram.com/mysecondteacher_nepal/`,
      youtube: `https://www.youtube.com/channel/UC6UzfTB3Psc3pJwQfh7-qkg`,
    },
    contact: {
      email: 'learn@advancedpedagogy.com',
    },
    menuLinks: [
      {
        name: 'About',
        link: '/about',
      },
      {
        name: 'Our Partners',
        link: '/partners',
      },
      {
        name: 'Intellectual Properties',
        link: '',
        linkList: [
          {
            name: 'APollo Platform',
            link: '/apollo-platform',
          },
          {
            name: 'IVy',
            link: '/ivy',
          },
          {
            name: 'mySecondTeacher',
            link: '/mysecondteacher',
          },
          {
            name: 'Classroom',
            link: '/classroom',
          },
          {
            name: 'eBook',
            link: '/ebook',
          },
          {
            name: 'Testpaper',
            link: '/testpaper',
          },
          {
            name: 'Group Chat Platform',
            link: '/group-chat-platform',
          },
        ],
      },
      {
        name: 'Contact',
        link: '/contact',
      },
    ],
    footerMenus: [
      {
        name: 'Product',
        link: '',
        linkList: [
          {
            name: 'APollo Platform',
            link: '/apollo-platform',
          },
          {
            name: 'IVy',
            link: '/ivy',
          },
          {
            name: 'mySecondTeacher',
            link: '/mysecondteacher',
          },
          {
            name: 'Classroom',
            link: '/classroom',
          },
          {
            name: 'eBook',
            link: '/ebook',
          },
          {
            name: 'Testpaper',
            link: '/testpaper',
          },
          {
            name: 'Group Chat Platform',
            link: '/group-chat-platform',
          },
        ],
      },
      {
        name: 'Company',
        link: '',
        linkList: [
          {
            name: 'About',
            link: '/about',
          },
          {
            name: 'Our Partners',
            link: '/partners',
          },
          {
            name: 'Career',
            link: '/career',
          },
        ],
      },
      {
        name: 'Legal',
        link: '',
        linkList: [
          {
            name: 'Privacy Policy',
            link: '/privacy-policy',
          },
          {
            name: 'Cookies Policy',
            link: '/cookies-policy',
          },
        ],
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-resolve-src',
      options: {
        srcPath: `${__dirname}/src`,
      },
    },
    'gatsby-plugin-sass',

    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        svgo: false,
        ref: true,
      },
    },
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: false, // Activates purging in npm run develop
        purgeOnly: ['/src/style.scss'], // applies purging only on the bootstrap css file
      },
    }, // must be after other CSS plugins
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/data`,
        name: `data`,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        // The option defaults to true
        checkSupportedExtensions: false,
      },
    },
    {
      resolve: 'gatsby-transformer-yaml-full',
      options: {
        plugins: [
          {
            resolve: 'gatsby-yaml-full-markdown',
            options: {
              /* gatsby-yaml-full-markdown options here */
            },
          },
        ],
      },
    },
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,
    // {
    //   resolve: `gatsby-plugin-sharp`,
    //   options: {
    //     defaults: {
    //       formats: [`auto`, `webp`],
    //       placeholder: `dominantColor`,
    //       quality: 50,
    //       breakpoints: [750, 1080, 1366, 1920],
    //       backgroundColor: `transparent`,
    //       tracedSVGOptions: {},
    //       blurredOptions: {},
    //       jpgOptions: {},
    //       pngOptions: {},
    //       webpOptions: {},
    //       avifOptions: {},
    //     },
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Advanced Pedagogy`,
        short_name: `adpeda`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#293F60`,
        display: `minimal-ui`,
        icon: `src/images/logo.svg`,
      },
    },
  ],
}
