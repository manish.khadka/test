import React, { useState } from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { useFlexSearch } from 'react-use-flexsearch'
import { Formik, Form, Field } from 'formik'

const SearchBar = () => {
  const data = useStaticQuery(graphql`
    {
      localSearchPages {
        index
        store
      }
    }
  `)

  const index = data.localSearchPages.index
  const store = data.localSearchPages.store
  const options = {
    tokenize: 'forward',
    suggestions: true,
  }
  const [query, setQuery] = useState(null)
  const [inputValue, setInputValue] = useState('')
  const results = useFlexSearch(query, index, store, options)
  const handleChange = (e) => {
    setInputValue(e.currentTarget.value)
    setQuery(e.currentTarget.value)
    // console.log(results)
  }

  return (
    <div className="px-2 my-3">
      <Formik
        initialValues={{ query: '' }}
        onSubmit={(values, { setSubmitting }) => {
          setQuery(values.query)
          setSubmitting(false)
        }}
      >
        <Form className="position-relative">
          <Field
            className="form-control"
            name="query"
            value={inputValue}
            onChange={handleChange}
          />
          {results.length > 0 && (
            <div
              className="position-absolute bg-white rounded shadow fadein"
              style={{ zIndex: 999 }}
            >
              <ul
                className="nav"
                style={{ maxHeight: '200px', overflow: 'auto' }}
              >
                {results.map((result) => (
                  <li key={result.id} className="border-bottom">
                    <Link to={result.slug} style={{ color: '#39C9A7' }}>
                      {result.title}
                    </Link>
                  </li>
                ))}
              </ul>
              <li>
                <Link to="/search" state={{ searchQuery: query }}>
                  View all results
                </Link>
              </li>
            </div>
          )}
        </Form>
      </Formik>
    </div>
  )
}

export default SearchBar
