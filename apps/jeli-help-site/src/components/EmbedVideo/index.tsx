/* eslint-disable-next-line */
export interface EmbedVideoProps {
  title: string
  embedVideoId: string
  isFullWidth: boolean
}

export function EmbedVideo(props: EmbedVideoProps) {
  return (
    <iframe
      title={props.title}
      src={`https://youtube.com/embed/${props.embedVideoId}`}
      className="embedVideo-iframe px-3 mb-3"
      loading="lazy"
      width={props.isFullWidth ? '100%' : '500'}
      height={props.isFullWidth ? '400' : '282'}
      frameBorder="0"
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen={true}
    ></iframe>
  )
}

export default EmbedVideo
