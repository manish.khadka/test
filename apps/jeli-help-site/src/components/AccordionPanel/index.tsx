import EmbedVideo from 'components/EmbedVideo'
/* eslint-disable-next-line */
export interface IndexProps {
  title: string
  id: string
  content: string
  embedVideoId: string
}

export function Index(props: IndexProps) {
  return (
    <div className="accordion-item panel panel-default mb-3">
      <div
        className="accordion-header panel-heading p-3"
        role="tab"
        id="heading1"
      >
        <div className="panel-title heading mb-0">
          <button
            className="accordion-button btn btn-link text-primary p-0 text-left"
            aria-expanded="true"
            aria-controls={props.id}
            data-bs-toggle="collapse"
            data-bs-target={`#${props.id}`}
          >
            {props.title}
          </button>
        </div>
      </div>
      <div
        id={props.id}
        className="accordion-collapse panel-collapse collapse show"
        role="tabpanel"
        aria-labelledby="heading1"
      >
        <div
          className="panel-body px-3 pt-3"
          dangerouslySetInnerHTML={{ __html: props.content }}
        />
        {props.embedVideoId && (
          <EmbedVideo
            title={props.title}
            embedVideoId={props.embedVideoId}
            isFullWidth={false}
          />
        )}
      </div>
    </div>
  )
}

export default Index
