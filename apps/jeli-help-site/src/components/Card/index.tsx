// import './card.module.scss';
import React from 'react'

const Card = ({ background, list, heading }) => {
  const getBackgrourdColor = (background: string) => {
    switch (background) {
      case 'gray':
        return '#f9f7ff'
      case 'green':
        return '#ebf9f6'
      case 'blue':
        return '#e5f1ff'
      default:
        return '#fffee6'
    }
  }
  return (
    <div
      className="card mb-4 card--title"
      style={{ backgroundColor: getBackgrourdColor(background) }}
    >
      <div className="card-body">
        <div className="d-flex align-items-center">
          <div
            className="heading-icon"
            style={{
              backgroundImage: `url(https://msthelpsite-production-landing-wordpress.s3.ap-southeast-1.amazonaws.com/2020/03/technical-support.svg)`,
            }}
          ></div>
          <h6 className="heading">{heading}</h6>
        </div>
        <div className="mt-2">
          <ul className="arrow__li-icon">
            {list.map((item) => (
              <li key={item}>{item}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}
export default Card
