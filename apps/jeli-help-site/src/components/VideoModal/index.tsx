// import React from 'react'
// import { logo as teacherImg } from '@ap-websites/shared-ui'
// import { logo as teacherImg } from '@ap-websites/assets/index.tsx'
import teacherImg from '@ap-websites/assets/index-flip.png'
import EmbedVideo from 'components/EmbedVideo'
// import teacherImg from '@ap-websites/assets/logo.svg'

/* eslint-disable-next-line */
export interface IndexProps {
  title: string
  id: string
  embedVideoId: string
}

export function Index(props: IndexProps) {
  return (
    <>
      {/* <img src={teacherImg} alt="" /> */}
      <div
        className="video-thumnail"
        data-bs-toggle="modal"
        data-bs-target={`#${props.id}`}
      >
        <img
          alt={props.title}
          src={`https://img.youtube.com/vi/${props.embedVideoId}/hqdefault.jpg`}
          className="rounded"
        />
      </div>
      {/* <button
        type="button"
        className="btn btn-primary mb-3"
        data-bs-toggle="modal"
        data-bs-target={`#${props.id}`}
      >
        {props.title}
      </button> */}
      <p className="mt-1 mb-3" id="group_2">
        {props.title}
      </p>

      <div
        className="modal fade"
        id={props.id}
        tabIndex={-1}
        aria-labelledby={`${props.id}Label`}
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered text-center">
          <div className="modal-content">
            {/* <div className="modal-header">
              <h5 className="modal-title" id={ `${props.id}Label` }>
                {props.title}
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div> */}
            <div className="modal-body">
              <button
                type="button"
                className="btn-close close"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true" className="p-1">
                  ×
                </span>
              </button>
              <EmbedVideo
                title={props.title}
                embedVideoId={props.embedVideoId}
                isFullWidth={true}
              />
              <p className="mt-1 mb-3" id="group_4">
                {props.title}
              </p>
            </div>
            {/* <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save changes
              </button>
            </div> */}
          </div>
        </div>
      </div>
    </>
  )
}

export default Index
