import React from 'react'
import { graphql } from 'gatsby'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import AccordionPanel from 'components/AccordionPanel'
import { uglifyString } from '@ap-websites/utils'

const BlogCollectionTemplate = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const page = data.markdownRemark
  const posts = data.allMarkdownRemark.nodes

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo
        title={page.frontmatter.title}
        description={page.frontmatter.description || page.excerpt}
      />
      <header>
        <h1 className="heading" itemProp="headline">
          {page.frontmatter.title}
        </h1>
      </header>
      <div className="accordion panel-group" id="accordion">
        {posts.map((result) => (
          <AccordionPanel
            key={result.id}
            id={uglifyString(result.frontmatter.title)}
            title={result.frontmatter.title}
            content={result.html}
            embedVideoId={result.frontmatter.embedVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogCollectionTemplate

export const pageQuery = graphql`
  query BlogCollectionBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
      }
    }
    allMarkdownRemark(
      filter: { fields: { slug: { regex: $slug, ne: $slug } } }
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      nodes {
        id
        fields {
          slug
        }
        html
        frontmatter {
          title
          embedVideoId
        }
      }
    }
  }
`
