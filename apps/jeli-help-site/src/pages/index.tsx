import React from 'react'
import { Link, graphql } from 'gatsby'
import Card from 'components/Card'
import Videomodal from 'components/VideoModal'
import AccordionPanel from 'components/AccordionPanel'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import { uglifyString } from '@ap-websites/utils'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heading = data.dataYaml.heading
  const description = data.dataYaml.description
  const subHeader = data.dataYaml.subHeader
  const cards = data.dataYaml.cards
  const featuredVideos = data.featuredVideos.nodes
  const faqTitle = data.dataYaml.faqTitle
  const hasFeaturedVideos = data.dataYaml.hasFeaturedVideos
  const featuredQuestions = data.featuredQuestions.nodes
  console.log(faqTitle)

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title="Home" />
      <h2 className="heading mb-3 bold">{heading}</h2>
      <h4>{subHeader}</h4>
      <p>{description}</p>
      <div className="row mb-4">
        {cards.map((result) => (
          <div className="col-lg-6" key={result.title}>
            <Card
              heading={result.title}
              list={result.list}
              background={result.bgcolor}
            />
          </div>
        ))}
      </div>
      {hasFeaturedVideos && (
        <div className="row mb-4">
          {featuredVideos.map((result) => (
            <div
              className="col-lg-4 col-md-6 col-sm-4 col-6"
              key={uglifyString(result.frontmatter.title)}
            >
              <Videomodal
                embedVideoId={result.frontmatter.embedVideoId}
                id={uglifyString(result.frontmatter.title)}
                title={result.frontmatter.title}
              />
            </div>
          ))}
        </div>
      )}

      <h4>{faqTitle}</h4>
      <div className="accordion panel-group" id="accordion">
        {featuredQuestions.map((result) => (
          <AccordionPanel
            key={uglifyString(result.frontmatter.title)}
            id={uglifyString(result.frontmatter.title)}
            title={result.frontmatter.title}
            content={result.html}
            embedVideoId={result.frontmatter.embedVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "homepage" }) {
      id
      heading
      subHeader
      description
      cards {
        title
        bgcolor
        list
      }
      hasFeaturedVideos
      faqTitle
    }
    featuredVideos: allMarkdownRemark(
      filter: { frontmatter: { isFeaturedVideo: { eq: true } } }
      sort: { fields: frontmatter___date, order: DESC }
    ) {
      nodes {
        id
        frontmatter {
          title
          embedVideoId
        }
      }
    }
    featuredQuestions: allMarkdownRemark(
      filter: { frontmatter: { isFeaturedQuestion: { eq: true } } }
      sort: { fields: frontmatter___date, order: DESC }
    ) {
      nodes {
        id
        frontmatter {
          title
          embedVideoId
        }
        html
      }
    }
  }
`
