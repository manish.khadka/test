import React, { useState } from 'react'
import { Link, graphql } from 'gatsby'
import { useFlexSearch } from 'react-use-flexsearch'
// import SearchBar from "components/search"
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import { Formik, Form, Field } from 'formik'
import AccordionPanel from 'components/AccordionPanel'
import { uglifyString } from '@ap-websites/utils'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const index = data.localSearchPages.index
  const store = data.localSearchPages.store
  //   const options = {
  //     tokenize: "forward",
  //     suggestions: true,
  //   }
  const initalSearch = location.state ? location.state.searchQuery : null
  const [query, setQuery] = useState(initalSearch)
  const [inputValue, setInputValue] = useState('')
  const results = useFlexSearch(query, index, store)
  const handleChange = (e) => {
    setInputValue(e.currentTarget.value)
    setQuery(e.currentTarget.value)
    console.log(results)
  }

  return (
    <Layout location={location} title={siteTitle} showNav={true}>
      <Seo title="Home" />
      {/* <SearchBar /> */}
      <Formik
        initialValues={{
          query: location.state ? location.state.searchQuery : '',
        }}
        onSubmit={(values, { setSubmitting }) => {
          setQuery(values.query)
          setSubmitting(false)
        }}
      >
        <Form>
          <Field
            className="form-control mb-4"
            name="query"
            value={inputValue}
            onChange={handleChange}
          />
        </Form>
      </Formik>
      <h1 className="mb-4">Search Results for {query}:</h1>
      {results.length < 1 && <p className="my-5">No results found</p>}
      <div className="accordion panel-group" id="accordion">
        {results.map((result) => (
          <AccordionPanel
            key={result.id}
            id={uglifyString(result.title)}
            title={result.title}
            content={result.body}
            embedVideoId={result.embedVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    localSearchPages {
      index
      store
    }
  }
`
