---
title: Can I change my email address later?
date: '2020-05-06T23:46:37.121Z'
category: Registration
isFeaturedQuestion: true
showInNav: true
embedVideoId: HfRK5M9BAYU
---

Yes, you can.

- Click on the top right icon of mySecondTeacher or your profile picture
- Click on **My Profile**.
- On the left side, click on the **Basic information** tab.

After that, you can change your **email address** and save it.
