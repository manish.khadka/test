---
title: Apa itu Jelajah Ilmu?
date: '2021-05-06T23:46:37.121Z'
category: Umum
isFeaturedQuestion: true
---

**Jelajah Ilmu** memberikan kemudahan khususnya dalam sistem belajar online &
offline di Indonesia, baik siswa maupun pengajar untuk mencapai proses belajar
mengajar yang optimal.

**Guru** dapat membuat dan melaksanakan kelas teks online atau live video,
memberikan, mengumpulkan, dan menilai tugas, mengakses berbagai eTextbook,
eWorkbook, eTeachersGuide serta Video interaktif. Guru juga dapat membuat Lembar
Ujian dan skema penilaian bersamaan dengan penilaian secara mendalam atas
kesulitan belajar siswa.

**Siswa** dapat belajar dari kelas daring yang dibuat oleh guru mereka. Mereka
juga dapat belajar dari video interaktif yang menyenangkan, eTextbook &
eWorkbook, mengikuti kuis untuk mendapatkan laporan diagnostik tentang kekuatan
dan kelemahan mereka. Tidak hanya itu, siswa juga dapat bersosialisasi dengan
guru dan teman sekelas mereka.

We also have 24×7 online tutor support who are ready to answer your problems and
help you with your learning.
