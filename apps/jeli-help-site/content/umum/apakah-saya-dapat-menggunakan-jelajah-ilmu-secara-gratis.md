---
title: Apakah saya dapat menggunakan Jelajah Ilmu secara gratis?
date: '2015-05-06T23:46:37.121Z'
category: Umum
isFeaturedQuestion: true
showInNav: false
order: 2
---

Untuk saat ini, Jelajah Ilmu hanya dapat diakses melalui kolaborasi dengan
sekolah. Informasikan kepada sekolah Anda tentang platform Jelajah Ilmu agar
Anda dapat menggunakan platform yang telah memenangkan penghargaan versi majalah
Education Technologies Insight pada tahun 2020 dan finalis EdTech Award pada
April 2021.
