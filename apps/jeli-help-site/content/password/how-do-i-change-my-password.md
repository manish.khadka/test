---
title: How do I change my password?
date: '2020-05-06T23:46:37.121Z'
category: Password
isFeaturedQuestion: true
isFeaturedVideo: true
showInNav: false
embedVideoId: HfRK5M9BAYU
---

- Click on the top right icon of mySecondTeacher or your profile picture
- Click on My Profile
- On the left side, click on the Basic information tab.
- Now you can also change your password and save it.
