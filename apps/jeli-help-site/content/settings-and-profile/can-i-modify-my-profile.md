---
title: Can I modify my profile?
date: '2020-05-06T23:46:37.121Z'
category: Settings and Profile
isFeaturedQuestion: true
isFeaturedVideo: true
showInNav: false
embedVideoId: HfRK5M9BAYU
---

Yes. To modify your profile:

- Login to your account.
- On the top right, click on your profile picture icon.
- On the menu that appears, click on My Profile.
- On the left side, you’ll see all the tabs you can modify. The settings that
  you can adjust are:
  - Basic Information
  - Password
  - Credits
  - Subscription
