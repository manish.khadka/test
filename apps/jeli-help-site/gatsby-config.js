module.exports = {
  siteMetadata: {
    title: `Pusat Bantuan`,
    author: {
      name: `jelajahilmu`,
      summary: `Your second Teacher :)`,
    },
    description: `Help Desk for jelajahilmu`,
    siteUrl: `https://help.jelajahilmu.com/`,
    social: {
      twitter: `MySecondTeache1`,
    },
    menuLinks: [
      {
        name: 'Tentang Jelajah Ilmu',
        link: 'https://jelajahilmu.com/tentang/',
      },
      {
        name: 'Siswa',
        link: 'https://jelajahilmu.com/siswa/',
      },
      {
        name: 'Guru',
        link: 'https://jelajahilmu.com/guru/',
      },
      {
        name: 'Kepala Sekolah',
        link: 'https://jelajahilmu.com/kepala-sekolah/',
      },
      {
        name: 'Orang Tua',
        link: 'https://jelajahilmu.com/orang-tua/',
      },
      {
        name: 'Hubungi Kami',
        link: 'https://jelajahilmu.com/hubungi-kami/',
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-resolve-src',
      options: {
        srcPath: `${__dirname}/src`,
      },
    },
    'gatsby-plugin-sass',
    // {
    //   resolve: `gatsby-plugin-sass`,
    //   options: {
    //     sassOptions: {
    //       includePaths: [`libs/shared-ui/src/styles`]
    //     }
    //   },
    // },
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        svgo: false,
        ref: true,
      },
    },
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: false, // Activates purging in npm run develop
        purgeOnly: ['/bootstrap.scss'], // applies purging only on the bootstrap css file
      },
    }, // must be after other CSS plugins
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content`,
        name: `content`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/data`,
        name: `data`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 630,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
              width: 500,
            },
          },
          {
            resolve: 'gatsby-remark-embed-video',
            options: {
              width: 500,
              ratio: 1.77, // Optional: Defaults to 16/9 = 1.77
              related: false, //Optional: Will remove related videos from the end of an embedded YouTube video.
              noIframeBorder: true, //Optional: Disable insertion of <style> border: 0
              loadingStrategy: 'lazy', //Optional: Enable support for lazy-load offscreen iframes. Default is disabled.
              urlOverrides: [
                {
                  id: 'youtube',
                  embedURL: (videoId) =>
                    `https://www.youtube-nocookie.com/embed/${videoId}`,
                },
              ], //Optional: Override URL of a service provider, e.g to enable youtube-nocookie support
              containerClass: 'embedVideo-container', //Optional: Custom CSS class for iframe container, for multiple classes separate them by space
              iframeId: false, //Optional: if true, iframe's id will be set to what is provided after 'video:' (YouTube IFrame player API requires iframe id)
            },
          },
          // "gatsby-remark-embed-video",
          // 'gatsby-remark-responsive-iframe',
          'gatsby-remark-prismjs',
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        // A unique name for the search index. This should be descriptive of
        // what the index contains. This is required.
        name: 'pages',

        // Set the search engine to create the index. This is required.
        // The following engines are supported: flexsearch, lunr
        engine: 'flexsearch',

        // Provide options to the engine. This is optional and only recommended
        // for advanced users.
        //
        // Note: Only the flexsearch engine supports options.
        engineOptions: 'speed',
        // tokenize: "forward",
        // suggest: true,
        // depth: 3,
        // limit: 5,

        // GraphQL query used to fetch all data for the search index. This is
        // required.
        query: `
          {
            allMarkdownRemark(
              filter: {rawMarkdownBody: {ne: ""}}
            ) {
              nodes {
                id
                frontmatter {
                  title
                  embedVideoId
                }
                html
                excerpt(pruneLength: 50, format: HTML)
                fields {
                  slug
                }
              }
            }
          }
        `,

        // Field used as the reference value for each document.
        // Default: 'id'.
        ref: 'id',

        // List of keys to index. The values of the keys are taken from the
        // normalizer function below.
        // Default: all fields
        index: ['title', 'body'],

        // List of keys to store and make available in your UI. The values of
        // the keys are taken from the normalizer function below.
        // Default: all fields
        store: ['id', 'slug', 'title', 'excerpt', 'body', 'embedVideoId'],

        // Function used to map the result from the GraphQL query. This should
        // return an array of items to index in the form of flat objects
        // containing properties to index. The objects must contain the `ref`
        // field above (default: 'id'). This is required.
        normalizer: ({ data }) =>
          data.allMarkdownRemark.nodes.map((node) => ({
            id: node.id,
            title: node.frontmatter.title,
            slug: node.fields.slug,
            body: node.html,
            embedVideoId: node.frontmatter.embedVideoId,
            excerpt: node.excerpt,
          })),
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    'gatsby-transformer-yaml',
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Jeli Help Site`,
        short_name: `Jeli Help`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#6E4BEC`,
        display: `minimal-ui`,
        icon: `static/favicon-32x32.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
