import React from 'react'
import { isEven, isOdd } from '@ap-websites/utils'
import { DynamicImage } from '@ap-websites/shared-ui'
/* eslint-disable-next-line */
export interface FeaturesListProps {
  list: Array<{
    title: string
    description: string
    img: string
    isComingSoon: boolean
  }>
  isNumbered: boolean
  startingBlockImgPostion: string
}

export function FeaturesList(props: FeaturesListProps) {
  const asdf = props.startingBlockImgPostion || 'right'
  const order = (order, index) => {
    if (order === 'left') return isOdd(index)
    if (order === 'right') return isEven(index)
    return isOdd(index)
  }
  return (
    <>
      {props.list.map((result, index) => (
        <div className="row align-items-center" key={index + 1}>
          <div
            className={`col-md-6 ${order(asdf, index) ? 'order-first' : ''}`}
          >
            <div
              className={`default  order-first ${
                order(asdf, index) ? 'left' : 'right'
              }`}
            >
              {props.isNumbered && (
                <p className="description-p font-size-l mb-1 text-story-list">
                  {index + 1}.
                </p>
              )}
              {result.isComingSoon && (
                <div
                  className="coming-soon-status position-absolute"
                  style={{ left: '6rem', top: '-0.2rem' }}
                >
                  <span className="badge badge-pill badge-primary">
                    Coming Soon
                  </span>
                </div>
              )}
              <h5 className="heading h4 text-grade">{result.title}</h5>
              <p className="description-p font-size-s">{result.description}</p>
            </div>
          </div>
          <div
            className={`col-md-6 default text-center ${
              order(asdf, index) ? '' : 'order-first'
            }`}
          >
            <figure className={`${order(asdf, index) ? 'right' : 'left'}`}>
              {/* <img src={result.img} alt={result.title} /> */}
              <DynamicImage imageData={result.img} alt={result.title} />
            </figure>
          </div>
        </div>
      ))}
    </>
  )
}

export default FeaturesList
