import React from 'react'
import { DynamicImage } from '@ap-websites/shared-ui'
import { StaticImage } from 'gatsby-plugin-image'

/* eslint-disable-next-line */
export interface JoinUsProps {
  data: {
    title: string
    img?: object
    ctaText: string
    ctaLink: string
  }
}

export function JoinUs(props: JoinUsProps) {
  return (
    <div className="row align-items-center">
      <div className="col-md-6">
        <h3 className="heading mb-4 h1  default ">{props.data.title}</h3>
        <div className="float-left">
          <a
            className="btn btn-blue px-5 btn btn-primary my-4"
            href={
              props.data.ctaLink ||
              'https://nepal-app.mysecondteacher.com/register'
            }
          >
            {props.data.ctaText}
          </a>
        </div>
      </div>
      <div className="col-md-6 default text-center ">
        <figure className="">
          {/* <img
            src="https://mysecondteacher.com.np/wp-content/uploads/2021/02/cta-kids.svg"
            alt={props.data.title || 'Get Started For Free'}
          /> */}
          {props.data.img ? (
            <DynamicImage />
          ) : (
            <StaticImage src="../../images/cta-kids.svg" alt={'Join Us'} />
          )}
        </figure>
      </div>
    </div>
  )
}

export default JoinUs
