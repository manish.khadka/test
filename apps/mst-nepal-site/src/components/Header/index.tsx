import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
// import logo from '@ap-websites/assets/images/mst-nepal.svg'
import logo from 'images/mst-nepal.svg'

const Header = ({ title }) => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          menuLinks {
            link
            name
            linkList {
              name
              link
            }
          }
        }
      }
    }
  `)
  const menuItems = data.site.siteMetadata.menuLinks
  function handleClick() {
    document.getElementById('topnav').classList.toggle('sidebar-nav--show')
    document
      .getElementById('topnav_hamburger')
      .classList.toggle('hamburger--cross')
  }
  return (
    <>
      <header className="navbar-wrapper main_navbar navbar-main navbar navbar-light navbar-expand fixed-top justify-content-between mx-auto px-1 p-0">
        <div className="container ml-20">
          <i className="navbar-brand position-absolute ml-3">
            <Link to="/">
              <img
                alt="logo"
                src={logo}
                className="img-fluid custom-logo"
                width="80"
              ></img>
            </Link>
          </i>
          <div className="d-flex align-items-center">
            <ul className="navbar-nav mt-1">
              {menuItems.map((result) => (
                <li
                  className={`nav-item menu-item menu-item-type-custom menu-item-object-custom ${
                    result.linkList ? 'always-show' : ''
                  }`}
                  key={result.name}
                >
                  <Link className="nav-link" target="" to={result.link}>
                    {result.name}
                    {result.linkList && (
                      <svg
                        className="dropdown-icon ml-2"
                        width="8"
                        height="10"
                        viewBox="0 0 16 10"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M6.34061 1.73531C7.13322 0.557245 8.86674 0.557243 9.65936 1.73531L15.0059 9.68178H0.99411L6.34061 1.73531Z"
                          fill="#FFFFFFBF"
                        ></path>
                      </svg>
                    )}
                  </Link>

                  {result.linkList && (
                    <div
                      className="dropdown-menu sub-menu mini-menu-wrapper py-0"
                      aria-labelledby="dropdownMenuButton"
                    >
                      <div className="container">
                        <div className="mini-menu">
                          {result.linkList.map((item, index) => (
                            <Link
                              key={index}
                              to={item.link}
                              activeClassName="current_page_item active"
                              className="mini-menu__item  menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1335    menu-item-2003 active "
                            >
                              <div className="mini-menu__text-wrapper">
                                <h5 className="heading">{item.name}</h5>
                                <small></small>
                              </div>
                            </Link>
                          ))}
                        </div>
                      </div>
                    </div>
                  )}
                </li>
              ))}
              {/* <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                <Link
                  className="nav-link active"
                  to="/"
                  activeClassName="active"
                >
                  Help
                </Link>
              </li> */}
            </ul>
          </div>
          <div className="d-flex align-items-center">
            <div className="nav__btn pl-2">
              <a
                target=""
                href="https://nepal-app.mysecondteacher.com/login"
                className="btn btn-white text-primary menu-item menu-item-type-custom menu-item-object-custom menu-item-1389"
              >
                Login
              </a>
            </div>
            <div className="nav__btn pl-2">
              <a
                target=""
                href="https://nepal-app.mysecondteacher.com/register"
                className="btn btn-primary--flat menu-item menu-item-type-custom menu-item-object-custom menu-item-1390"
              >
                Register <small>for Free</small>
              </a>
            </div>
          </div>
        </div>
      </header>
      <header className="navbar main_navbar mobile-nav--wrapper navbar-light bg-white sticky-top">
        <i className="navbar-brand ml-3">
          <Link to="/">
            <img alt="logo" src={logo} className="img-fluid" width="80"></img>
          </Link>
        </i>

        <div className="d-flex align-items-center">
          <div
            className="hamburger pr-2"
            id="topnav_hamburger"
            onClick={() => handleClick()}
          >
            <span className="line"></span>
            <span className="line"></span>
            <span className="line"></span>
          </div>
        </div>

        <div className="mobile-nav mt-2 pt-3 mx-auto shadow" id="topnav">
          <div className="container-fluid">
            <div className="row mobile-nav__menu-ul px-4 pt-1">
              <ul className="navbar-nav mt-1">
                {menuItems.map((result) => (
                  <li
                    className="nav-item menu-item menu-item-type-custom menu-item-object-custom"
                    key={result.name}
                  >
                    <Link className="nav-link" to={result.link}>
                      {result.name}
                    </Link>
                  </li>
                ))}
                <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                  <Link
                    className="nav-link active"
                    to="/"
                    activeClassName="active"
                  >
                    Help
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}
export default Header
