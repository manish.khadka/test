import React from 'react'
import { DynamicImage } from '@ap-websites/shared-ui'

/* eslint-disable-next-line */
export interface FeaturesGridProps {
  list: Array<{
    title: string
    description: string
    img: string
    isComingSoon: boolean
  }>
}

export function FeaturesGrid(props: FeaturesGridProps) {
  return (
    <>
      {props.list.map((result, index) => (
        <div
          className={`${
            props.list.length === 1 ? 'col-md-10' : 'col-md-4'
          } mb-5 px-4 ${
            (index + 1) % 3 === 0 || index + 1 === props.list.length
              ? ''
              : 'border-md-right'
          }`}
          key={result.title}
        >
          <div className="pl-md-4">
            {result.isComingSoon && (
              <div className="coming-soon-status position-absolute">
                <span className="badge badge-pill badge-primary">
                  Coming Soon
                </span>
              </div>
            )}
            {result.img && (
              <figure className="text-center">
                {/* <img
                  className="w-md-100"
                  src={result.img}
                  height="130"
                  alt={result.title}
                /> */}
                <DynamicImage
                  imageData={result.img}
                  alt={result.title}
                  className="w-md-100"
                  imageStyles={{ height: '130px' }}
                />
              </figure>
            )}
          </div>
          <div className="pl-md-4 text-center">
            <article className="w-100">
              <h5
                className={`heading ${
                  props.list.length === 1 ? 'font-size-l' : 'font-size-md'
                }`}
              >
                {result.title}
              </h5>
              <p className="font-size-s description-p">{result.description}</p>
            </article>
          </div>
        </div>
      ))}
    </>
  )
}

export default FeaturesGrid
