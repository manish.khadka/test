import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import socialIconFacebook from 'images/social-facebook.svg'
import socialIconInstagram from 'images/social-instagram.svg'
import socialIconYoutube from 'images/social-youtube.svg'
import contactIconEmail from 'images/contact-email.svg'
import contactIconPhone from 'images/contact-phone.svg'
import brandIcon from 'images/mst-nepal.svg'
/* eslint-disable-next-line */
export interface FooterProps {}

export function Footer(props: FooterProps) {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          contact {
            email
            numbers
          }
          footerMenuLinks {
            link
            name
          }
        }
      }
    }
  `)
  const menuItems = data.site.siteMetadata.footerMenuLinks
  const contact = data.site.siteMetadata.contact
  return (
    <footer className="section-footer pt-4 pb-2">
      <div className="container">
        <div className="pb-2 row justify-content-center">
          <div className="col-lg-3 col-md-6 col-sm-6 pt-4">
            <div className="widget_text footer__ul">
              <div className="textwidget custom-html-widget">
                <figure>
                  <img alt="" src={brandIcon} width="100" />
                </figure>
                <div className="d-flex m-2">
                  <a
                    href="https://www.facebook.com/mySecondTeacherNepal/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="pr-4">
                      <img src={socialIconFacebook} alt="icon-facebook" />
                    </span>
                  </a>
                  <a
                    href="https://www.instagram.com/mysecondteacher_nepal/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="pr-4">
                      <img src={socialIconInstagram} alt="icon-instagram" />
                    </span>
                  </a>
                  <a
                    href="https://www.youtube.com/c/MySecondTeacherNepal"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="pr-4">
                      <img src={socialIconYoutube} alt="icon-youtube" />
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-6 pt-4">
            <div className="footer__ul">
              <small className="text-uppercase text-white-50">About</small>
              <div className="menu-footer-about-menu-container">
                <ul id="menu-footer-about-menu" className="menu">
                  {menuItems.map((result, index) => (
                    <li
                      key={index}
                      id="menu-item-1476"
                      className="right-triangle-pointer menu-item menu-item-type-post_type menu-item-object-page menu-item-1476"
                    >
                      <Link to={result.link}> {result.name}</Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6 col-sm-6 pt-4"></div>
          <div className="col-lg-3 col-md-6 col-sm-6 pt-4">
            <div className="widget_text footer__ul">
              <small className="text-uppercase text-white-50">
                Let’s connect!
              </small>
              <div className="textwidget custom-html-widget">
                <ul className="pl-0">
                  <li>
                    <a className="pl-0" href={`mailto:${contact.email}`}>
                      <img
                        src={contactIconEmail}
                        alt="icon-email"
                        className="mr-2"
                        style={{ marginTop: '-2px' }}
                      />
                      {contact.email}
                    </a>
                  </li>
                  {contact.numbers.map((result, index) => (
                    <li className="mb-1" key={index}>
                      <a className="pl-0" href={`tel:${result}`}>
                        <img
                          src={contactIconPhone}
                          alt="icon-phone"
                          style={{ marginTop: '-2px' }}
                          className={`mr-2 ${index === 0 ? '' : 'invisible'} `}
                        />
                        {result}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom mt-3 mb-3 d-flex justify-content-center">
          <div className="d-flex align-items-center">
            <small className="d-block mt-1">
              © mySecondTeacher {new Date().getFullYear()}
            </small>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
