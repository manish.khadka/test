import React from 'react'
import { uglifyString } from '@ap-websites/utils'
import { Link } from 'gatsby'
import { DynamicImage } from '@ap-websites/shared-ui'
import { StaticImage } from 'gatsby-plugin-image'

/* eslint-disable-next-line */
export interface ExploreTabsProps {
  list: Array<{
    title: string
    tagline: string
    description: string
    ctaLink: string
    icon: string
    media: string
    mediaType: string
  }>
}

export function ExploreTabs(props: ExploreTabsProps) {
  return (
    <>
      <div className="col-12 d-flex justify-content-center mb-5">
        <ul className="nav nav-pills" role="tablist" id="exploreTabs">
          {props.list.map((result, index) => (
            <li className="nav-item" key={index}>
              <a
                className={`nav-link ${index === 0 ? 'active' : ''}`}
                data-bs-toggle="pill"
                href={`#${uglifyString(result.title)}`}
              >
                {result.title}
              </a>
            </li>
          ))}
        </ul>
      </div>
      <div className="col-12 d-flex mb-4">
        <div className="m-2 d-flex flex-column">
          <div className="tab-content" id="exploreTabsContent">
            {props.list.map((result, index) => (
              <div
                key={index}
                id={uglifyString(result.title)}
                className={`container tab-pane ${index === 0 ? 'active' : ''}`}
              >
                <div className="row">
                  <div className="col-md-5">
                    <div className="row">
                      <div className="col mb-1">
                        <figure
                          className="mst-help__bg-img p-0"
                          style={{
                            backgroundSize: '100%',
                            backgroundColor: '#6e4bec',
                          }}
                        >
                          <DynamicImage
                            imageData={result.icon}
                            className="rounded-lg"
                            alt={''}
                            style={{ backgroundColor: '#6e4bec' }}
                          />
                        </figure>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">
                        <h5 className="heading text-white">{result.tagline}</h5>
                        <p className="color-body font-size-m text-white mb-5">
                          {result.description}
                        </p>
                        <Link
                          to={result.ctaLink}
                          className="btn btn-primary--alt py-2 px-4 mt-auto mb-4"
                        >
                          Learn More
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-5 offset-md-2">
                    <div className="media--large d-none d-md-flex">
                      <figure>
                        <StaticImage
                          src={'../../images/Rectangle-531-e1625491211437.png'}
                          className="rounded-lg"
                          alt={''}
                          style={{ width: '100%', height: '100%' }}
                        />
                        {/* <img
                          loading="lazy"
                          className="rounded-lg"
                          src="https://mysecondteacher.com.np/wp-content/uploads/2021/04/Rectangle-531-e1625491211437.png"
                          width="100%"
                          height="100%"
                          alt=""
                        /> */}
                      </figure>
                      <div className="media--small d-none d-md-flex">
                        <figure>
                          {result.mediaType === 'video' ? (
                            <video
                              muted={true}
                              loop={true}
                              autoPlay={true}
                              className="rounded-lg mt-5"
                            >
                              <source
                                src={result.media.publicURL}
                                type="video/mp4"
                              />
                            </video>
                          ) : (
                            <DynamicImage
                              imageData={result.media}
                              className="rounded-lg"
                              alt={''}
                              style={{ width: '100%', height: '100%' }}
                            />
                            // <img
                            //   loading="lazy"
                            //   className="rounded-lg"
                            //   src={result.media}
                            //   width="100%"
                            //   height="100%"
                            //   alt=""
                            // />
                          )}
                        </figure>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  )
}

export default ExploreTabs
