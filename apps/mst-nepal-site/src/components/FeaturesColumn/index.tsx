import React from 'react'
import { DynamicImage } from '@ap-websites/shared-ui'

/* eslint-disable-next-line */
export interface FeaturesColumnProps {
  list: Array<{
    title: string
    description: string
    icon: string
  }>
}

export function FeaturesColumn(props: FeaturesColumnProps) {
  return (
    <>
      {props.list.map((result, index) => (
        <div className="col-md-3 col-sm-10 mb-lg-5 mb-3 column-panel-block">
          <div className="column-panel-sub-block">
            <figure className="rounded p-1">
              {/* <img alt="" className="w-md-100 img-fluid" src="" /> */}
              {/* <DynamicImage
                  imageData={result.img}
                  alt={result.title}
                  className="w-md-100 img-fluid"
                /> */}
            </figure>
            <h6 className="h6 heading font-weight-bold mb-2 d-flex">
              {result.title}
              <span className="mr-2 order-first">i</span>
            </h6>
            <p>{result.description}</p>
          </div>
        </div>
      ))}
    </>
  )
}

export default FeaturesColumn
