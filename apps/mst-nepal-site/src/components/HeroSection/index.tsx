import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import { DynamicImage } from '@ap-websites/shared-ui'
/* eslint-disable-next-line */
export interface HeroSectionProps {
  data: {
    title: string
    artwork: object
    description: string
  }
}

export function HeroSection(props: HeroSectionProps) {
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-10">
          <h3 className="heading mb-4 text-center h1 mb-5  default ">
            {props.data.title}
          </h3>
          <figure className="text-center mb-5">
            <DynamicImage
              imageData={props.data.artwork}
              alt={props.data.title}
            />
          </figure>
          <div className="default order-first">
            <p
              className="text-center"
              dangerouslySetInnerHTML={{ __html: props.data.description }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default HeroSection
