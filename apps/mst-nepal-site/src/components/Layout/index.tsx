import React from 'react'
import Header from 'components/Header'
import Footer from 'components/Footer'

const Layout = ({ location, title, children, isFullWidth }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath

  return (
    <>
      <Header title={title} />
      <div className={isFullWidth ? `container-fluid` : `container`}>
        <main
          className={`section--padded-t ${isRootPath ? 'home' : ''}`}
          data-is-root-path={isRootPath}
        >
          <main>{children}</main>
        </main>
      </div>
      <Footer />
    </>
  )
}

export default Layout
