import React from 'react'
import { Link, graphql } from 'gatsby'
import { DynamicImage } from '@ap-websites/shared-ui'

import Layout from 'components/Layout'
import tickmark from 'images/tickmark.svg'
import Seo from 'components/Seo'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const statsSection = data.dataYaml.statsSection
  const downloadSection = data.dataYaml.downloadSection
  const lmsSection = data.dataYaml.lmsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section
        className="section-padding position-relative section--padded-y--md full-width acf-block"
        style={{ backgroundColor: '#f2f8ff' }}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10">
              <h3 className="heading mb-4 text-center  default ">
                {heroSection.title}
              </h3>
              <div className=" default   order-first">
                <p style={{ textAlign: 'center' }}>{heroSection.description}</p>
              </div>
              <div className="mt-5 mb-5">
                <div
                  role="form"
                  className="wpcf7"
                  id="wpcf7-f2373-o1"
                  lang="en-US"
                  dir="ltr"
                >
                  <div className="screen-reader-response">
                    <p role="status" aria-live="polite" aria-atomic="true"></p>{' '}
                    <ul></ul>
                  </div>
                  <form
                    action="/our-solutions#wpcf7-f2373-o1"
                    method="post"
                    className="wpcf7-form init"
                    data-status="init"
                  >
                    <div className="row d-flex justify-content-center">
                      <div className="col-12 col-lg-10">
                        <div className="form-row">
                          <div className="col-md-8">
                            <span className="wpcf7-form-control-wrap contact_email">
                              <input
                                type="email"
                                name="contact_email"
                                value=""
                                className="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control py-2"
                                aria-required="true"
                                aria-invalid="false"
                                placeholder="Email Address"
                              />
                            </span>
                          </div>
                          <div className="col-md-4 mt-3 pl-md-3 mt-md-0 justify-content-center justify-content-md-start">
                            <input
                              type="submit"
                              value="Apply Now"
                              className="wpcf7-form-control wpcf7-submit btn btn-primary py-2"
                            />
                            <span className="ajax-loader"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="wpcf7-response-output" aria-hidden="true"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section-padding position-relative section--padded-y--md full-width acf-block">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10">
              <figure className="our-solutions-hero-image">
                {/* <img src={statsSection.image} alt="" /> */}
                <DynamicImage imageData={statsSection.image} alt="" />
              </figure>
              <div className="row">
                {statsSection.list.map((result, index) => (
                  <div className="col-lg-4" key={index}>
                    <div className="card--panel py-3 px-2 my-3 rounded-lg">
                      <div className="row">
                        <div className="col-12 d-flex">
                          <div className="card--image mr-3">
                            <figure>
                              {/* <img
                                className="img-fluid"
                                src={result.img}
                                width="120px"
                                alt=""
                              /> */}
                              <DynamicImage
                                imageData={result.img}
                                alt=""
                                className="img-fluid"
                              />
                            </figure>
                          </div>
                          <div className="card--description d-flex justify-content-center flex-column">
                            <h3 className="heading text-left">
                              {result.title}
                            </h3>
                            <div className="description-p font-size-s text-left">
                              <p>{result.description}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div
                className="full-width mt-4"
                style={{ backgroundColor: '#1b80e3' }}
              >
                <div className="row flex-row justify-content-end">
                  <div
                    className="col-lg-9 section--padded-y--md position-relative"
                    style={{ backgroundColor: '#f2f8ff' }}
                  >
                    <div className="section_image h-100 d-none d-lg-block">
                      <figure className="" style={{ width: '295px' }}>
                        <DynamicImage imageData={downloadSection.img} alt="" />

                        {/* <img
                          src=""
                          width="295px"
                          alt=""
                        /> */}
                      </figure>
                    </div>
                    <div className="container">
                      <div className="row">
                        <div className="col-lg-9 offset-lg-2">
                          <h5 className="heading h1">
                            {downloadSection.title}
                          </h5>
                          <div className="generic-p">
                            <p>{downloadSection.description}</p>
                          </div>
                          <div className="row mt-5">
                            {downloadSection.list.map((result, index) => (
                              <div className="col-lg-6 mb-3" key={index}>
                                <div className="row">
                                  <div className="col-12 d-flex justify-content-start justify-content-lg-center my-2">
                                    <div className="list-tick-mark mr-3 d-flex justify-content-center">
                                      <div className="list-tick-mark--wrapper rounded">
                                        <img src={tickmark} alt="" />
                                      </div>
                                    </div>
                                    <div className="list-tick-mark-description">
                                      <h5 className="heading">
                                        {result.title}
                                      </h5>
                                      <div className="description-p font-size-s">
                                        {result.description}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ))}
                          </div>
                          <div className="row mt-4">
                            <div className="col-12 d-flex flex-row">
                              {downloadSection.links.map((result, index) => (
                                <figure className="mr-4" key={index}>
                                  <a href={result.href}>
                                    {/* <img src={result.image} alt={result.type} /> */}
                                    <DynamicImage
                                      imageData={result.image}
                                      alt={result.type}
                                    />
                                  </a>
                                </figure>
                              ))}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="section-padding position-relative section--padded-y--md full-width acf-block pt-0">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10">
              <h3 className="heading mb-4 h5 text-center  default ">
                {lmsSection.title}
              </h3>
              <div className=" default   order-first">
                <p style={{ textAlign: 'center' }}>{lmsSection.description}</p>
              </div>
              <div className="row mt-5">
                {lmsSection.list.map((result, index) => (
                  <div className="col-lg-4" key={index}>
                    <div className="row">
                      <div className="col-3 d-flex justify-content-end justify-content-lg-end">
                        <figure>
                          {/* <img
                            className="list-tick-mark"
                            src={result.img}
                            alt={result.title}
                          /> */}
                          <DynamicImage
                            imageData={result.img}
                            alt={result.title}
                          />
                        </figure>
                      </div>
                      <div className="col-9">
                        <h5 className="heading h5">{result.title}</h5>
                        <div className="description-p font-size-s">
                          <p>{result.description}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "our-solutions" }) {
      id
      heroSection {
        title
        cta
        description
      }
      statsSection {
        image {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 1000)
          }
          extension
          publicURL
        }
        list {
          description
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR, width: 120)
            }
            extension
            publicURL
          }
        }
      }
      downloadSection {
        description
        title
        img {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 295)
          }
          extension
          publicURL
        }
        links {
          image {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          href
          type
        }
        list {
          description
          title
        }
      }
      lmsSection {
        description
        title
        list {
          description
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          title
        }
      }
    }
  }
`
