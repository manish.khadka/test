import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
import JoinUs from 'components/JoinUs'
import FeaturesList from 'components/FeaturesList'
import ImageSlider from 'components/ImageSlider'
import HeroSection from 'components/HeroSection'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const highlightsSection = data.dataYaml.highlightsSection
  const imageSliderSection = data.dataYaml.imageSliderSection
  const joinUsSection = data.dataYaml.joinUsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section className="section-padding position-relative section--padded-y--md full-width bg-primary-gradient-light">
        <HeroSection data={heroSection} />
      </section>
      <section className="section--padded-y py-4 default acf-block py-5">
        <div className="container position-relative">
          <div className="row justify-content-center ">
            <div className="col-lg-10">
              <ImageSlider imgList={imageSliderSection.imgList} />
            </div>
          </div>
        </div>
      </section>
      <section
        className="section-padding position-relative section--padded-y--md full-width pb-0"
        style={{ backgroundColor: '#f2f8ff' }}
      >
        <div className="container">
          <FeaturesList
            list={highlightsSection.highlights}
            isNumbered={false}
            startingBlockImgPostion={'left'}
          />
        </div>
      </section>
      <section className="section-padding position-relative section--padded-y--md full-width section--padded-y--md bottom-fold-bg">
        <div className="container">
          <JoinUs data={joinUsSection} />
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "parents" }) {
      id
      heroSection {
        title
        artwork {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        description
      }
      imageSliderSection {
        imgList {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 1000)
          }
          extension
          publicURL
        }
      }
      highlightsSection {
        highlights {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          description
          isComingSoon
        }
      }
      joinUsSection {
        title
        ctaText
      }
    }
  }
`
