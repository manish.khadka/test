import React from 'react'
import { Link, graphql } from 'gatsby'
import { GatsbyImage, getImage, StaticImage } from 'gatsby-plugin-image'
import { DynamicImage } from '@ap-websites/shared-ui'

import Layout from 'components/Layout'
import Seo from 'components/Seo'

import FeaturesList from 'components/FeaturesList'
import FeaturesGrid from 'components/FeaturesGrid'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const quoteSection = data.dataYaml.quoteSection
  const storySection = data.dataYaml.storySection
  const approachSection = data.dataYaml.approachSection
  const technologySection = data.dataYaml.technologySection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section className="section-padding position-relative section--padded-y--md">
        <div className="row align-items-center">
          <div className="col-md-6">
            <div className=" default order-first">
              <div className="mt-5">
                <figure className="about-intro__quotation">
                  {/* <img
                    src="https://mysecondteacher.com.np/wp-content/uploads/2021/02/Combined-Shape.svg"
                    alt=""
                    width="80"
                  /> */}
                  <DynamicImage
                    imageData="https://mysecondteacher.com.np/wp-content/uploads/2021/02/Combined-Shape.svg"
                    alt="quote"
                    imageStyle={{ width: '80px' }}
                  />
                </figure>
                <p>{quoteSection.quote}</p>
                <p>
                  <strong>– {quoteSection.author}</strong>
                  <br />
                  {quoteSection.designation}
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6 order-first text-center ">
            {/* <figure className="">
              <img
                src="https://mysecondteacher.com.np/wp-content/uploads/2021/02/Group-15.png"
                alt={quoteSection.author}
              />
            </figure> */}
            {/* <StaticImage
              src="../images/Group-15.png"
              alt="test"
              formats={['auto', 'webp', 'avif']}
            /> */}
            {/* <GatsbyImage image={getImage(quoteSection.img)} alt="" /> */}
            <DynamicImage
              imageData={quoteSection.img}
              alt={quoteSection.author}
            />
          </div>
        </div>
      </section>
      <section
        className="section-padding position-relative section--padded-y--md full-width pb-0"
        style={{ backgroundColor: '#f2f8ff' }}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10">
              <h3 className="heading mb-4 heading h3 text-center  default ">
                {storySection.title}
              </h3>
            </div>
          </div>
          <FeaturesList
            list={storySection.list}
            isNumbered={true}
            startingBlockImgPostion={'right'}
          />
        </div>
      </section>
      <section className="section--padded-t pb-5 full-width " id="our-approach">
        <div className="container">
          <div className="row justify-content-center text-center">
            <div className="col-xl-7 text-center">
              <h5 className="heading text-center h3">
                {approachSection.title}
              </h5>
              <p className="generic-p mb-5"></p>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row p-3 justify-content-center">
                <FeaturesGrid list={approachSection.list} />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section
        className="section--padded-t pt-5 position-relative overflow-hidden full-width mb-5"
        id="our-technology"
      >
        <div
          className="section--padded-t bg-color-blue color-white"
          style={{
            backgroundImage: 'url(' + technologySection.bgImg.publicURL + ')',
          }}
        >
          <div className="container section--padded-y">
            <div className="row">
              <div className="col-md-10 mx-auto text-center">
                <h2 className="heading mb-4 color-white">
                  {technologySection.title}
                </h2>
                {technologySection.description}
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mx-auto">
                <figure className="about__our-tech-figure pt-lg-5 pb-lg-4 position-relative">
                  {technologySection.list.map((result) => (
                    <div
                      className="about__our-tech-img d-inline"
                      key={result.title}
                    >
                      <figure className="d-inline-block">
                        {/* {!result.img.childImageSharp &&
                        result.img.extension === 'svg' ? (
                          <img
                            style={{ width: '200px' }}
                            src={result.img.publicURL}
                            alt={result.title}
                          />
                        ) : (
                          <GatsbyImage
                            style={{ width: '200px' }}
                            image={getImage(result.img)}
                            alt={result.title}
                          />
                        )} */}
                        <DynamicImage
                          imageData={result.img}
                          alt={result.title}
                          imageStyle={{ width: '200px' }}
                        />
                        <figcaption className="text-white text-center mt-n3">
                          {result.title}
                        </figcaption>
                      </figure>
                    </div>
                  ))}

                  <div className="about__our-tech-img">
                    {/* static path to asset */}
                    <DynamicImage
                      imageData="/images/connectorx.svg"
                      alt="connector"
                    />
                  </div>
                </figure>
              </div>
            </div>
          </div>
          <div className="about__our-tech-bg-svg"> </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "about" }) {
      id
      quoteSection {
        quote
        author
        designation
        img {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 550)
          }
          extension
          publicURL
        }
      }
      storySection {
        title
        list {
          title
          img {
            childImageSharp {
              gatsbyImageData
            }
            extension
            publicURL
          }
          description
        }
      }
      approachSection {
        title
        list {
          title
          img {
            childImageSharp {
              gatsbyImageData
            }
            extension
            publicURL
          }
          description
        }
      }
      technologySection {
        title
        bgImg {
          publicURL
        }
        connectorImg {
          publicURL
        }
        description
        list {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG, width: 200)
            }
            extension
            publicURL
          }
        }
      }
    }
  }
`
