import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
import FeaturesList from 'components/FeaturesList'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const highlightsSection = data.dataYaml.highlightsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section className="position-relative section--padded-t full-width acf-block home-enroll--bluebgn">
        <figure
          className="hero-pattern-icons"
          id="sub-hero-pattern-icons"
          style={{
            transform: 'translate3d(0px, 0px, 0px) rotate(0.0001deg)',
            transformStyle: 'preserve-3d',
            backfaceVisibility: 'hidden',
            pointerEvents: 'none',
          }}
        >
          <div
            data-depth="0.2"
            style={{
              transform: 'translate3d(12.4px, -7.8px, 0px)',
              transformStyle: 'preserve-3d',
              backfaceVisibility: 'hidden',
              position: 'relative',
              display: 'block',
              left: '0px',
              top: '0px',
            }}
          >
            <img
              alt=""
              className="w-100"
              id="hero-react"
              src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/ico-hero-react.png"
            />
          </div>
          <div
            data-depth="0.06"
            style={{
              transform: 'translate3d(3.7px, -2.4px, 0px)',
              transformStyle: 'preserve-3d',
              backfaceVisibility: 'hidden',
              position: 'absolute',
              display: 'block',
              left: '0px',
              top: '0px',
            }}
          >
            <img
              alt=""
              className="w-100"
              id="hero-compass"
              src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/ico-hero-note.png"
            />
          </div>
        </figure>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-xl-3 col-lg-3 mx-auto">
              <div className="row pb-1 ">
                {heroSection.subjectList.slice(0, 2).map((result, index) => (
                  <div
                    className="col-xl-10 col-lg-12 col-md-4 col-sm-5 col-5 d-flex mb-4 "
                    key={index}
                  >
                    <div className="m-2 d-flex">
                      <div className="rounded-lg bg-transparent position-relative transformed-text-wrapper ">
                        <video
                          className="rounded-lg mouse-over"
                          loop={true}
                          muted={true}
                          src={result.videoUrl}
                          poster={result.thumbnail}
                          preload="metadata"
                        ></video>
                        <p className="font-size-s position-absolute transformed-text">
                          {result.title}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-10 col-sm-12 mx-auto text-center justify-content-center mb-4">
              <h2 className="heading pr-1 mb-4">{heroSection.title}</h2>
              <p className="mb-4">{heroSection.description}</p>
              <Link
                to="/about"
                className="btn btn-primary font-weight-bold py-2 px-4 mt-auto mb-4"
              >
                {heroSection.cta}
              </Link>
            </div>
            <div className="col-xl-3 col-lg-3 mx-auto">
              <div className="row pb-1 ">
                {heroSection.subjectList.slice(2, 4).map((result, index) => (
                  <div
                    className="col-xl-10 col-lg-12 col-md-4 col-sm-5 col-5 d-flex mb-4 "
                    key={index}
                  >
                    <div className="m-2 d-flex">
                      <div className="rounded-lg bg-transparent position-relative transformed-text-wrapper ">
                        <video
                          className="rounded-lg mouse-over"
                          loop={true}
                          muted={true}
                          src={result.videoUrl}
                          poster={result.thumbnail}
                          preload="metadata"
                        ></video>
                        <p className="font-size-s position-absolute transformed-text">
                          {result.title}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section
        className="section-padding position-relative section--padded-y--md full-width pb-0"
        style={{ backgroundColor: '#f2f8ff' }}
      >
        <div className="container">
          <FeaturesList
            list={highlightsSection.highlights}
            isNumbered={false}
            startingBlockImgPostion={''}
          />
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "our-contents" }) {
      id
      heroSection {
        title
        cta
        description
        subjectList {
          title
          thumbnail
          videoUrl
        }
      }
      highlightsSection {
        highlights {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          description
          isComingSoon
        }
      }
    }
  }
`
