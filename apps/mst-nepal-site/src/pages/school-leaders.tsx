import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
import FeaturesColumn from 'components/FeaturesColumn'
import JoinUs from 'components/JoinUs'
import ImageSlider from 'components/ImageSlider'
import { InteractiveArtwork, DynamicImage } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const featuresSection = data.dataYaml.featuresSection
  const featuresColumnSection = data.dataYaml.featuresColumnSection
  const imageSliderSection = data.dataYaml.imageSliderSection
  const artworkSection = data.dataYaml.artworkSection
  const joinUsSection = data.dataYaml.joinUsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section
        className={`section-padding position-relative section--padded-y--md full-width bg-primary-gradient-light ${
          heroSection.hasArtworkbg ? 'mountain-artwork-bg' : ''
        }`}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-10">
              <h3 className="heading mb-4 text-center h1 mb-5  default ">
                {heroSection.title}
              </h3>
              <figure
                className={`text-center mb-5 ${
                  heroSection.hasArtworkbg ? 'invisible' : ''
                }`}
              >
                <DynamicImage
                  imageData={heroSection.artwork}
                  alt={heroSection.title}
                />
                {/* <img src={heroSection.artwork} alt={heroSection.title} /> */}
              </figure>
              <div className=" default   order-first">
                <p className="text-center">{heroSection.description}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-t pb-5 full-width ">
        <div className="container">
          <div className="row justify-content-center text-center">
            <div className="col-xl-7 text-center">
              <h5 className="heading text-center h3">
                {featuresSection.title}
              </h5>
              <p className="generic-p mb-5">{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row p-3 justify-content-center">
                <FeaturesGrid list={featuresSection.features} />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-y py-4 default acf-block py-5">
        <div className="container position-relative">
          <div className="row justify-content-center ">
            <div className="col-lg-10">
              <ImageSlider imgList={imageSliderSection.imgList} />
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-t full-width">
        <InteractiveArtwork image={artworkSection.image} />
      </section>
      <section className="section--padded-t pb-5 full-width bg-color-lightblue mb-3 mb-md-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="row p-3 justify-content-center">
                <FeaturesColumn list={featuresColumnSection.features} />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section-padding position-relative section--padded-y--md full-width section--padded-y--md bottom-fold-bg">
        <div className="container">
          <JoinUs data={joinUsSection} />
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "school-leaders" }) {
      id
      heroSection {
        title
        hasArtworkbg
        artwork {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        description
      }
      imageSliderSection {
        imgList {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 1000)
          }
          extension
          publicURL
        }
      }
      artworkSection {
        image {
          childImageSharp {
            gatsbyImageData
          }
          extension
          publicURL
        }
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          description
        }
      }
      featuresColumnSection {
        features {
          title
          description
        }
      }
      joinUsSection {
        title
        ctaText
      }
    }
  }
`
