import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
import ExploreTabs from 'components/ExploreTabs'
import { EmbedVideo, DynamicImage } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const exploreSection = data.dataYaml.exploreSection
  const unleashSection = data.dataYaml.unleashSection
  const videoSection = data.dataYaml.videoSection
  const featuresSection = data.dataYaml.featuresSection
  const testimonialsSection = data.dataYaml.testimonialsSection
  const downloadSection = data.dataYaml.downloadSection
  //   const forList = data.dataYaml.forList

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section className="position-relative overflow-hidden full-width bg-img pb-0">
        <div className="row h-100 mr-0">
          <div className="col hero-main--left">
            <div className="hero-absolute-img">
              {/* <img
                alt=""
                className="img-fluid h-100"
                src={heroSection.artwork}
                width="100%"
              /> */}
              <DynamicImage imageData={heroSection.artwork} alt={''} />
            </div>
            <div className="container">
              <div
                id="hero-main-img"
                style={{
                  transform: 'translate3d(0px, 0px, 0px) rotate(0.0001deg)',
                  transformStyle: 'preserve-3d',
                  backfaceVisibility: 'hidden',
                  pointerEvents: 'none',
                }}
              >
                <figure
                  className="m-0"
                  data-depth="0.04"
                  style={{
                    transform: 'translate3d(0.6px, -0.1px, 0px)',
                    transformStyle: 'preserve-3d',
                    backfaceVisibility: 'hidden',
                    position: 'relative',
                    display: 'block',
                    left: '0px',
                    top: '0px',
                  }}
                >
                  {/* <img
                    alt=""
                    className="img-fluid"
                    src={heroSection.img}
                    width="780"
                  /> */}
                  <DynamicImage
                    imageData={heroSection.img}
                    alt={''}
                    className="img-fluid"
                  />
                </figure>
              </div>
              <div className="container text-center ">
                <div className="section--padded-t mx-auto">
                  <div className="row">
                    <div className="col-12">
                      <div className="text-center pt-1 mx-auto text-black-50 mt-4">
                        <h1 className="heading h1 mb-3">{heroSection.title}</h1>
                        <div className="row justify-content-center">
                          <div className="col-9">
                            <p className="generic-p">
                              {heroSection.description}
                            </p>
                          </div>
                        </div>
                        <a
                          href={heroSection.ctaHref}
                          className="btn btn-primary--alt py-2 px-4 mt-auto mb-4"
                        >
                          {heroSection.ctaText}
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="invisible">
                    <figure className="hero-main-img position-relative mb-0">
                      {/* <img
                        alt=""
                        className="img-fluid"
                        src={heroSection.img}
                        width="680"
                      /> */}
                      <DynamicImage
                        imageData={heroSection.img}
                        alt={''}
                        className="img-fluid"
                      />
                    </figure>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 hero-main--right">
            <div className="container">
              <div className="section--padded-t mx-auto">
                <div className="row">
                  <div className="col-12">
                    <div className="text-center pt-1 mb-3 mx-auto">
                      <h1 className="heading h3 text-white">
                        {heroSection.subHeader}
                      </h1>
                    </div>
                    <div className="row justify-content-center">
                      {heroSection.forList.map((result, index) => (
                        <div className="col-6 mb-4" key={index}>
                          <figure className="d-flex justify-content-center">
                            <Link
                              className="margin-left-auto "
                              to={result.link}
                            >
                              <DynamicImage
                                className="img-fluid aos-init aos-animate"
                                imageData={result.img}
                                alt=""
                              />
                            </Link>
                          </figure>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="explore-section section--padded-t full-width position-relative">
        <DynamicImage
          imageData={exploreSection.bgImg}
          alt=""
          className="position-absolute"
          imageStyle={{ top: '0', left: '0', right: '0', bottom: '0' }}
        />

        <div className="container">
          <div className="text-center justify-content-center">
            <h2 className="heading pr-1 heading--alt text-white">
              {exploreSection.title}
            </h2>
          </div>
          <div className="row">
            <div className="col-xl-11 mx-auto">
              <div className="row pb-1">
                <ExploreTabs list={exploreSection.list} />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="full-width">
        <div className="bg-img-unleash position-relative">
          {/* <DynamicImage
            imageData={unleashSection.bgTop}
            alt=""
            className="position-absolute"
            imageStyle={{ top: '0', left: '0', right: '0', bottom: '0' }}
          /> */}
          <div
            className="bg-image-wrapper position-absolute"
            style={{ top: '0', left: '0', right: '0', bottom: '0' }}
          >
            <DynamicImage
              imageData={unleashSection.bgTop}
              alt=""
              className="position-absolute"
              imageStyle={{ inset: 0 }}
            />
          </div>
          <figure className="mb-0 mt-auto bg-img-unleash-white-overlay w-100">
            {/* <img
              alt=""
              className="w-100"
              src="https://mysecondteacher.com.np/wp-content/uploads/2019/06/unleash-white.png"
            /> */}
            <DynamicImage
              imageData={unleashSection.bgMask}
              className="w-100"
              alt={''}
            />
          </figure>
        </div>
        <div className="position-relative bg-img unleashed-section">
          <div
            className="bg-image-wrapper position-absolute"
            style={{ top: '0', left: '0', right: '0', bottom: '0' }}
          >
            <DynamicImage
              imageData={unleashSection.bgBottom}
              alt=""
              className="position-absolute"
              imageStyle={{ bottom: '0' }}
            />
          </div>
          <figure className="unleash-artwork">
            <img className="img-fluid" src="" width="180" alt="" />
          </figure>
          <div className="container unleash-text">
            <div className="row">
              <div className="offset-md-1 col-sm-10 col-md-7 order-md-1 order-2">
                <article className="mb-md-4 mb-5 px-2 unleash-content">
                  <h1 className="heading font-secondary font-weight-normal">
                    {unleashSection.title}
                  </h1>
                  <p>{unleashSection.description}</p>
                  <div className="mt-4">
                    <a
                      href={unleashSection.ctaHref}
                      className="btn btn-primary--alt btn-rounded px-5"
                    >
                      {unleashSection.ctaText}
                    </a>
                  </div>
                </article>
              </div>
              <div className="offset-md-2 offset-8 col-md-3 order-1 order-md-2 invisible d-none d-md-block">
                <figure>
                  <img className="img-fluid" src="" width="180" alt="" />
                </figure>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-y full-width bg-color-lightblue position-relative">
        <figure
          className="video-pattern-icons"
          id="sub-video-pattern-icons"
          style={{
            transform: 'translate3d(0px, 0px, 0px) rotate(0.0001deg)',
            transformStyle: 'preserve-3d',
            backfaceVisibility: 'hidden',
            pointerEvents: 'none',
          }}
        >
          <div
            data-depth="0.2"
            style={{
              transform: 'translate3d(23.6px, -15.3px, 0px)',
              transformStyle: 'preserve-3d',
              backfaceVisibility: 'hidden',
              position: 'relative',
              display: 'block',
              left: '0px',
              top: '0px',
            }}
          >
            <img
              alt=""
              className="w-100"
              id="hero-react"
              src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/ico-hero-react.png"
            />
          </div>

          <div
            data-depth="0.06"
            style={{
              transform: 'translate3d(7.1px, -4.6px, 0px)',
              transformStyle: 'preserve-3d',
              backfaceVisibility: 'hidden',
              position: 'absolute',
              display: 'block',
              left: '0px',
              top: '0px',
            }}
          >
            <img
              alt=""
              className="w-100"
              id="hero-compass"
              src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/ico-hero-note.png"
            />
          </div>
        </figure>
        <div className="container position-relative">
          <div className="row justify-content-center">
            <div className="col-md-7">
              <h5 className="heading--semi font-size-l text-center">
                {videoSection.title}
              </h5>
              <p className="generic-p font-size-s mb-5 text-center">
                {videoSection.description}
              </p>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-10 d-flex justify-content-center">
              <div
                className="video-thumnail w-100"
                id="video-thumbnail"
                data-toggle="modal"
                data-target="#video1"
                data-video={videoSection.videoId}
              >
                <img
                  alt={videoSection.videoId}
                  src={`https://i3.ytimg.com/vi/${videoSection.videoId}/maxresdefault.jpg`}
                  className="rounded-lg img-fluid w-100"
                />
              </div>
              <div
                className="video-content embed-responsive embed-responsive-16by9 w-100"
                id="video-content"
              >
                <EmbedVideo
                  title={videoSection.title}
                  embedVideoId={videoSection.videoId}
                  isFullWidth={true}
                  id="contentVideo"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-t pb-5 full-width ">
        <div className="container">
          <div className="row justify-content-center text-center">
            <div className="col-xl-7 text-center">
              <h5 className="heading text-center h3">
                {featuresSection.title}
              </h5>
              <p className="generic-p mb-5">{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row p-3 justify-content-center">
                <FeaturesGrid list={featuresSection.features} />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-y full-width bg-color-lightblue">
        <div className="container position-relative">
          <figure className="students-say__quotation">
            <img
              src="https://mysecondteacher.com.np/wp-content/themes/mst-develop/assets/images/quotation_grey.svg"
              alt=""
            />
          </figure>
          <div className="row justify-content-center">
            <div className="col-md-7">
              <h5 className="heading--semi mb-5 text-center">
                {testimonialsSection.title}
              </h5>
              <div className="students-say__slides slick-initialized slick-slider slick-dotted">
                <div
                  className="slick-list draggable"
                  style={{ height: '268px' }}
                >
                  <div
                    className="slick-track"
                    style={{
                      opacity: '1',
                      width: '635px',
                      transform: 'translate3d(0px, 0px, 0px)',
                    }}
                  >
                    <div
                      className="bg-color-white rounded-lg p-4 my-1 mx-2 slick-slide slick-current slick-active"
                      style={{ minHeight: '260px', width: '619px' }}
                      data-slick-index="0"
                      aria-hidden="false"
                      role="tabpanel"
                      id="slick-slide00"
                      aria-describedby="slick-slide-control00"
                    >
                      <div className="container">
                        <div className="row">
                          <div className="col-2">
                            <figure className="text-center">
                              <img
                                loading="lazy"
                                className="img-fluid rounded-circle mx-auto mt-1"
                                height="40"
                                src="https://mysecondteacher.com.np/wp-content/uploads/2021/04/Student_2x.png"
                                width="90"
                                alt=""
                              />
                            </figure>
                          </div>
                          <div className="col-9">
                            <div className="pt-1">
                              <div>
                                <span className="font-weight-bold">
                                  Samayee Kafle
                                </span>
                                <small className="text-muted">(Nepal)</small>
                              </div>
                              <div className="mb-3">
                                <small className="text-grade">Grade 10</small>
                              </div>
                              <p>
                                The amazing range of features like Test paper
                                generation really helped me prepare for my exams
                                even when I had to do it by myself.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section-padding position-relative section--padded-y--md full-width n-mb-5">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-6">
              <h3 className="heading mb-4 heading h2  default ">
                {downloadSection.title}
              </h3>
              <div className=" default   order-first">
                <p>{downloadSection.description}</p>
                <div className="d-flex mt-3">
                  {downloadSection.links.map((result, index) => (
                    <div className="mr-4" key={index}>
                      <a href={result.link} target="_blank" rel="noreferrer">
                        {/* <img src={result.image} alt={result.type} /> */}
                        <DynamicImage
                          imageData={result.image}
                          alt={result.type}
                        />
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="col-md-6 order-first text-center ">
              <figure className="width-70 mb-5 mb-md-0 d-none d-md-flex">
                {/* <img src={downloadSection.img} alt={downloadSection.title} /> */}
                <DynamicImage
                  imageData={downloadSection.img}
                  alt={downloadSection.title}
                />
              </figure>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "homepage" }) {
      id
      heroSection {
        artwork {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG, layout: FULL_WIDTH)
          }
          extension
          publicURL
        }
        ctaHref
        ctaText
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG, width: 2000)
          }
          extension
          publicURL
        }
        subHeader
        forList {
          link
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
        }
        title
      }
      exploreSection {
        title
        bgImg {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        list {
          title
          tagline
          description
          ctaLink
          icon {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR, width: 160)
            }
            extension
            publicURL
          }
          media {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR, width: 440)
            }
            extension
            publicURL
          }
          mediaType
        }
      }
      unleashSection {
        bgBottom {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        bgTop {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH, placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        bgMask {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, layout: FULL_WIDTH)
          }
          extension
          publicURL
        }
        ctaHref
        ctaText
        description
        title
      }
      videoSection {
        description
        title
        videoId
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          description
          isComingSoon
        }
      }
      testimonialsSection {
        testimonials {
          grade
          name
          testimonial
        }
        title
      }
      downloadSection {
        title
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR, width: 750)
          }
          extension
          publicURL
        }
        links {
          type
          image {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          link
        }
      }
    }
  }
`
