import React from 'react'
import Layout from 'components/Layout'
import Seo from 'components/Seo'

const NotFoundPage = (location) => (
  <Layout location={location} title={'Not Found'} isFullWidth>
    <Seo title="Not Found" />
    <section className="section pt-5 section--bg-light">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <div className="heading py-10 text-center h3">
              Sorry, we couldn't find what you're looking for.
            </div>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default NotFoundPage
