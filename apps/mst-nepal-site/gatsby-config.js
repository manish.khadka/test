require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})
module.exports = {
  siteMetadata: {
    title: `mySecondTeacher Nepal`,
    author: {
      name: `mySecondTeacher`,
      summary: `Your second Teacher :)`,
    },
    description: `mySecondTeacher Nepal`,
    siteUrl: `https://mysecondteacher.com.np/`,
    apiBaseUrl: `${process.env.GATSBY_API_URL}`,
    social: {
      twitter: `MySecondTeache1`,
      facebook: `https://www.facebook.com/mySecondTeacherNepal/`,
      instagram: `https://www.instagram.com/mysecondteacher_nepal/`,
      youtube: `https://www.youtube.com/c/MySecondTeacherNepal`,
    },
    contact: {
      email: 'nepal@mysecondteacher.com',
      numbers: ['+977-9801010144', '+977-9801010144'],
    },
    menuLinks: [
      {
        name: 'About',
        link: '/about',
      },
      {
        name: 'Our Solutions',
        link: '/our-solutions',
      },
      {
        name: 'Our Users',
        link: '',
        linkList: [
          {
            name: 'Student',
            link: '/students',
          },
          {
            name: 'Teachers',
            link: '/teachers',
          },
          {
            name: 'School Leaders',
            link: '/school-leaders',
          },
          {
            name: 'Parents',
            link: '/parents',
          },
          {
            name: 'Educators',
            link: '/educators',
          },
        ],
      },
      {
        name: 'Our Contents',
        link: '/our-contents',
      },
    ],
    footerMenuLinks: [
      {
        name: 'About',
        link: '/about',
      },
      {
        name: 'Student',
        link: '/students',
      },
      {
        name: 'Teachers',
        link: '/teachers',
      },
      {
        name: 'School Leaders',
        link: '/school-leaders',
      },
      {
        name: 'Parents',
        link: '/parents',
      },
      {
        name: 'Educators',
        link: '/educators',
      },
      {
        name: 'Partner',
        link: '/partner',
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-resolve-src',
      options: {
        srcPath: `${__dirname}/src`,
      },
    },
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        svgo: false,
        ref: true,
      },
    },
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: false, // Activates purging in npm run develop
        purgeOnly: ['/bootstrap.scss'], // applies purging only on the bootstrap css file
      },
    }, // must be after other CSS plugins
    // {
    //   resolve: `gatsby-source-filesystem`,
    //   options: {
    //     path: `${__dirname}/content`,
    //     name: `content`,
    //   },
    // },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/data`,
        name: `data`,
      },
    },
    // {
    //   resolve: `gatsby-transformer-remark`,
    //   options: {
    //     plugins: [
    //       {
    //         resolve: `gatsby-remark-images`,
    //         options: {
    //           maxWidth: 630,
    //         },
    //       },
    //       {
    //         resolve: `gatsby-remark-responsive-iframe`,
    //         options: {
    //           wrapperStyle: `margin-bottom: 1.0725rem`,
    //           width: 500,
    //         },
    //       },
    //       {
    //         resolve: 'gatsby-remark-embed-video',
    //         options: {
    //           width: 500,
    //           ratio: 1.77, // Optional: Defaults to 16/9 = 1.77
    //           related: false, //Optional: Will remove related videos from the end of an embedded YouTube video.
    //           noIframeBorder: true, //Optional: Disable insertion of <style> border: 0
    //           loadingStrategy: 'lazy', //Optional: Enable support for lazy-load offscreen iframes. Default is disabled.
    //           urlOverrides: [
    //             {
    //               id: 'youtube',
    //               embedURL: (videoId) =>
    //                 `https://www.youtube-nocookie.com/embed/${videoId}`,
    //             },
    //           ], //Optional: Override URL of a service provider, e.g to enable youtube-nocookie support
    //           containerClass: 'embedVideo-container', //Optional: Custom CSS class for iframe container, for multiple classes separate them by space
    //           iframeId: false, //Optional: if true, iframe's id will be set to what is provided after 'video:' (YouTube IFrame player API requires iframe id)
    //         },
    //       },
    //       // "gatsby-remark-embed-video",
    //       // 'gatsby-remark-responsive-iframe',
    //       'gatsby-remark-prismjs',
    //       `gatsby-remark-copy-linked-files`,
    //       `gatsby-remark-smartypants`,
    //     ],
    //   },
    // },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    // `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        // The option defaults to true
        checkSupportedExtensions: false,
      },
    },
    {
      resolve: 'gatsby-transformer-yaml-full',
      options: {
        plugins: [
          {
            resolve: 'gatsby-yaml-full-markdown',
            options: {
              /* gatsby-yaml-full-markdown options here */
            },
          },
        ],
      },
    },
    // 'gatsby-transformer-yaml',
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,
    // {
    //   resolve: `gatsby-plugin-sharp`,
    //   options: {
    //     defaults: {
    //       formats: [`auto`, `webp`],
    //       placeholder: `dominantColor`,
    //       quality: 50,
    //       breakpoints: [750, 1080, 1366, 1920],
    //       backgroundColor: `transparent`,
    //       tracedSVGOptions: {},
    //       blurredOptions: {},
    //       jpgOptions: {},
    //       pngOptions: {},
    //       webpOptions: {},
    //       avifOptions: {},
    //     },
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `MST Nepal Site`,
        short_name: `MST Nepal`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#6E4BEC`,
        display: `minimal-ui`,
        icon: `static/favicon-32x32.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
