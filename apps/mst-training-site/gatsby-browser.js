/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
// custom typefaces
// import "typeface-montserrat"
// import "typeface-merriweather"
// normalize CSS across browsers
import '@ap-websites/typeface-proxima-nova-soft'
import './src/normalize.css'
// custom CSS styles
// import './src/style.css'
import './src/bootstrap.css'

// Highlighting for code blocks
import 'prismjs/themes/prism.css'
