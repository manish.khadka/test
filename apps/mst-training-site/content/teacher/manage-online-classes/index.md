---
title: Manage Online Classes
date: 2015-01-08T23:46:37.121Z
description: Learn how to create, schedule and conduct live online classes.
category: Teacher
showInNav: true
---

_Learn how to create, schedule and conduct live online classes._

`youtube: AIK4wudR-38?rel=0`
