---
title: Create Test Paper
date: 2015-01-08T23:56:37.121Z
description: Learn how to generate custom TestPaper along with Marking Schemes
category: Teacher
showInNav: true
---

_Learn how to generate custom TestPaper along with Marking Schemes_

`youtube: -d2KF_-jDh8?rel=0`

If you have additional queries, then feel free to contact our support team via our Online Chat feature. Our support team are available 24/7 for you. You can access the online chat even without logging in.
