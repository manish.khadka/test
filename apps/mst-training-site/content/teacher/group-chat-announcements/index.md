---
title: Group Chat & Announcements
date: 2015-01-08T23:58:37.121Z
description:
  Learn how to communicate to your student(s) via group chat, digital notice
  boards, and make announcements.
category: Teacher
showInNav: true
---

_Learn how to communicate to your student(s) via group chat, digital notice
boards, and make announcements._

`youtube: zh4PXTWl4RE?rel=0`
