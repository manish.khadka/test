---
title: Review Student Performance
date: 2015-01-08T23:50:37.121Z
description: Learn how to access and understand student’s Diagnostic Reports.
category: Teacher
showInNav: true
---

_Learn how to access and understand student’s Diagnostic Reports._

`youtube: ggUs0VjYF_w?rel=0`
