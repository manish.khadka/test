---
title: Manage Assignments
date: 2015-01-08T23:55:37.121Z
category: Teacher
description:
  Learn how to create, give out assignments, receive, grade and publish the
  grades of submitted assignments.
showInNav: true
---

_Learn how to create, give out assignments, receive, grade and publish the
grades of submitted assignments._

`youtube: T0s-M0DxXEk?rel=0`
