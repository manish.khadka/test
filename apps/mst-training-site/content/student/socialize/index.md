---
title: Socialize

date: '2015-02-05T23:46:37.121Z'
category: Student
showInNav: true
description:
  Learn how to attend Teacher’s Live sessions and chat with your friends and
  teachers.
---

_Learn how to attend Teacher’s Live sessions and chat with your friends and
teachers._

`youtube: 718fTRmav0Q?rel=0`
