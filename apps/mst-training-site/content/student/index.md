---
title: Training Guide for Student
date: 2015-02-01T23:46:37.121Z
description:
  Welcome to the mySecondTeacher Training Guide for Students. Here, you will
  learn how to use our key features like – creating, scheduling and conducting
  live online classes, managing assignments, generating testpapers and marking
  schemes, communicating to students via group chat, etc.  Watch the full
  training video here. You can also select to watch training videos for specific
  features from the menu.
category: Student
showInNav: false
---

Welcome to the **mySecondTeacher** Training Guide for Students. Here, you will
learn how to use our key features like – creating, scheduling and conducting
live online classes, managing assignments, generating testpapers and marking
schemes, communicating to students via group chat, etc.

**Watch the full training video here. You can also select to watch training
videos for specific features from the menu.**

`youtube: tHbizq8wWSo?rel=0`
