---
title: Receive & Submit Assignments
date: '2015-02-06T23:46:37.121Z'
category: Student
showInNav: true
description:
  Learn how to receive assignments, submit them, and receive grades and remarks
  from your teachers on mySecondTeacher.
---

_Learn how to receive assignments, submit them, and receive grades and remarks
from your teachers on mySecondTeacher._

`youtube: OLlMkR44yJc?rel=0`
