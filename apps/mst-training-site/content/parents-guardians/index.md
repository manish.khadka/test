---
title: Training Guide for Parents/Guardians
date: '2018-05-06T23:46:37.121Z'
category: Parents/Guardians
description:
  Watch the full training video here. You can also select to watch training
  videos for specific features from the menu.
showInNav: false
---

Watch the full training video here. You can also select to watch training videos
for specific features from the menu.

#### Training Guide videos for School Leader are coming soon.
