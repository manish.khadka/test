import React from 'react'

// import * as styles from './index.module.scss'

// import React from 'react';
import { graphql } from 'gatsby'
import Card from '../components/card'
import HomeNav from '../components/homeNav'
import Layout from '../components/layout'
import Seo from '../components/seo'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`

  return (
    <Layout location={location} title={siteTitle}>
      <Seo title="Home" />
      <Card />
      <HomeNav />
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
