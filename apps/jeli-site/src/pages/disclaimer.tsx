import React from 'react'
import { graphql } from 'gatsby'
import Layout from 'components/Layout'
import Seo from 'components/Seo'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const pageTitle = data.dataYaml.pageTitle || `Title`
  const pageData = data.dataYaml.pageData
  return (
    <Layout location={location} title={pageTitle} isFullWidth>
      <Seo title={pageTitle} />
      <section className="section--padded-y">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-10 col-12">
              <div className="mb-4">
                <h1 className="heading mb-4">{pageTitle}</h1>
                <p
                  dangerouslySetInnerHTML={{
                    __html: pageData,
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "disclaimer" }) {
      id
      pageTitle
      pageData
    }
  }
`
