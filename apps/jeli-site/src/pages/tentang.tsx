import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
// import ExploreTabs from 'components/ExploreTabs'
import { EmbedVideo } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'
import HeroSection from 'components/HeroSection'
import { DynamicImage } from '@ap-websites/shared-ui'
import { StaticImage } from 'gatsby-plugin-image'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const highlightsSection = data.dataYaml.highlightsSection

  const featuresSection = data.dataYaml.featuresSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section className="section--padded-t position-relative full-width">
        <HeroSection
          title={heroSection.title}
          isTwoColumn={true}
          description={heroSection.description}
          img={heroSection.img}
        />
      </section>

      <section className="section--padded-y full-width bg-color-lightgrey">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 text-center mb-4 mx-auto">
              <h1 className="heading mb-3">{highlightsSection.title}</h1>
            </div>
          </div>
          <div className="row justify-content-center min-height-img mb-3">
            <FeaturesGrid list={highlightsSection.highlights} />
          </div>
        </div>
      </section>

      <section className="section--padded-y">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 text-center mb-4 mx-auto">
              <h1 className="heading mb-3">{featuresSection.title}</h1>
              <p>{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row justify-content-center min-height-img">
            <FeaturesGrid list={featuresSection.features} />
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "tentang" }) {
      id
      heroSection {
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }

        title
      }
      highlightsSection {
        title
        highlights {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
            extension
            publicURL
          }
          description
        }
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
            extension
            publicURL
          }
          description
        }
      }
    }
  }
`
