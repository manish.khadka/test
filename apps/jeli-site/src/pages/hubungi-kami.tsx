import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
// import ExploreTabs from 'components/ExploreTabs'
import { EmbedVideo } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'
import HeroSection from 'components/HeroSection'
import { DynamicImage } from '@ap-websites/shared-ui'
import { GatsbyImage, StaticImage } from 'gatsby-plugin-image'
import { Field, Form, Formik } from 'formik'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section className="section--padded-y position-relative full-width">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6">
              <h1
                className="heading mb-5 h1 default"
                dangerouslySetInnerHTML={{
                  __html: heroSection.title,
                }}
              />
              <Formik
                initialValues={{
                  query: location.state ? location.state.searchQuery : '',
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(false)
                }}
              >
                <Form>
                  <Field
                    className="form-control mb-4"
                    name="name"
                    placeholder="Nama"
                  />
                  <Field
                    className="form-control mb-4"
                    name="email"
                    placeholder="Alalmat Email"
                  />
                  <Field
                    className="form-control mb-4"
                    name="telephone"
                    placeholder="Telepon"
                  />
                  <Field
                    as="textarea"
                    className="form-control mb-4"
                    name="description"
                    placeholder="Pesan"
                  />
                  <div className=" default ">
                    <p
                      dangerouslySetInnerHTML={{
                        __html: heroSection.description,
                      }}
                    />
                  </div>
                  <input
                    type="submit"
                    value="Kirim"
                    className=" btn btn-primary btn-rounded px-5"
                  ></input>
                </Form>
              </Formik>
            </div>
            <div className="col-md-6">
              <DynamicImage imageData={heroSection.img} alt={''} />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "hubungi-kami" }) {
      id
      heroSection {
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: BLURRED)
          }
          extension
          publicURL
        }

        title
        ctaText
      }
    }
  }
`
