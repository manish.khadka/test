import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
// import ExploreTabs from 'components/ExploreTabs'
import { EmbedVideo } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'
import HeroSection from 'components/HeroSection'
import { DynamicImage } from '@ap-websites/shared-ui'
import { StaticImage } from 'gatsby-plugin-image'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const exploreSection = data.dataYaml.exploreSection

  const featuresSection = data.dataYaml.featuresSection

  const joinUsSection = data.dataYaml.joinUsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section className="section--padded-t position-relative full-width acf-block">
        <HeroSection
          title={heroSection.title}
          isTwoColumn={false}
          description={heroSection.description}
          img={heroSection.img}
        />
      </section>
      <section className="section--padded-y">
        <div className="container">
          <div className="text-center mb-4">
            <h1 className="heading">{exploreSection.title}</h1>
            <p
              dangerouslySetInnerHTML={{
                __html: exploreSection.description,
              }}
            />
          </div>
          <div className="row">
            <div className="col-xl-11 mx-auto">
              <div className="row justify-content-center pb-1 ">
                {exploreSection.list.map((result, index) => (
                  <div
                    className="col-lg-5 col-md-8 col-sm-10 d-flex mb-4"
                    key={index}
                  >
                    <div className="m-2 d-flex">
                      <div className="rounded-lg bg-color-white card">
                        <figure className="user-list__bg-img w-75 mx-auto p-0">
                          <DynamicImage
                            imageData={result.img}
                            imageStyle={{ aspectRatio: '390/224' }}
                          />
                        </figure>
                        <article className="h-100 text-center d-flex flex-column align-items-center card__content-box">
                          <h5 className="heading text-uppercase mb-2 mt-2">
                            {result.title}
                          </h5>
                          <p className="color-body mb-5">
                            {result.description}
                          </p>
                          <Link
                            to={result.ctaLink}
                            className="btn btn-primary py-2 px-4 mt-auto"
                          >
                            Cari Tahu
                          </Link>
                        </article>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-y">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 text-center mb-4 mx-auto">
              <h1 className="heading mb-3">{featuresSection.title}</h1>
              <p>{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row justify-content-center min-height-img">
            <FeaturesGrid list={featuresSection.features} col={3} />
          </div>
        </div>
      </section>
      <section className="section--padded mb-3">
        <div className="col-10 col-md-10 col-lg-8 col-xl-6 text-center mb-4 mx-auto">
          <div
            className="orbits position-relative"
            style={{ borderRadius: '50%' }}
          >
            <div
              className="d-none d-sm-block rotate position-relative"
              style={{ borderRadius: '50%', paddingTop: '100%' }}
            >
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '84%', left: '15%' }}
              >
                <StaticImage
                  alt="mercury"
                  className="pop img-size"
                  src="../images/orbit-mercury@2x.png"
                />
              </div>
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '2%', left: '25%' }}
              >
                <StaticImage
                  alt="venus"
                  className="pop img-size"
                  src="../images/orbit-venus@2x.png"
                />
              </div>
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '14%', left: '91%' }}
              >
                <StaticImage
                  alt="earth"
                  className="pop-alt img-size"
                  src="../images/orbit-earth@2x.png"
                />
              </div>
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '64%', left: '92%' }}
              >
                <StaticImage
                  alt="mars"
                  className="pop img-size"
                  src="../images/orbit-mars@2x.png"
                />
              </div>
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '39%', left: '-1%' }}
              >
                <StaticImage
                  alt="jupiter"
                  className="pop-alt img-size"
                  src="../images/orbit-jupiter@2x.png"
                />
              </div>
              <div
                className="rotate-reverse position-absolute "
                style={{ top: '97%', left: '67%' }}
              >
                <StaticImage
                  alt="saturn"
                  className="pop-alt img-size"
                  src="../images/orbit-saturn@2x.png"
                />
              </div>
            </div>
            <div
              className="position-absolute"
              style={{
                top: '50%',
                left: '50%',
                transform: 'translate(-50%,-50%)',
                width: '300px',
              }}
            >
              <div className="text-center mb-4">
                <h3 className="heading mb-3 h3">
                  Mulailah perjalanan belajar Anda!
                </h3>
                <p></p>
                <div className="mt-4">
                  <Link
                    to="/hubungi-kami"
                    className="btn btn-primary btn-rounded px-5"
                  >
                    Memulai
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "homepage" }) {
      id
      heroSection {
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }

        title
      }
      exploreSection {
        title
        description
        list {
          title

          description
          ctaLink
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
            extension
            publicURL
          }
        }
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
            extension
            publicURL
          }
          description
        }
      }

      joinUsSection {
        title
        description
        img
        ctaText
        ctaLink
      }
    }
  }
`
