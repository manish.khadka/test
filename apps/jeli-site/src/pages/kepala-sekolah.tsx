import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
// import ExploreTabs from 'components/ExploreTabs'
import FeaturesGrid from 'components/FeaturesGrid'
import HeroSection from 'components/HeroSection'
import { StaticImage } from 'gatsby-plugin-image'
import JoinUs from 'components/JoinUs'
import { ImageSlider, InteractiveArtwork } from '@ap-websites/shared-ui'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection

  const imageSliderSection = data.dataYaml.imageSliderSection
  const artworkSection = data.dataYaml.artworkSection
  const featuresSection = data.dataYaml.featuresSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Guru" />
      <section className="section--padded-t position-relative full-width">
        <HeroSection
          title={heroSection.title}
          isTwoColumn={false}
          description={heroSection.description}
          img={heroSection.img}
        />
      </section>
      <section className="section--padded-y py-4 default acf-block py-5">
        <div className="container position-relative">
          <div className="row justify-content-center ">
            <div className="col-lg-10">
              <ImageSlider imgList={imageSliderSection.imgList} />
            </div>
          </div>
        </div>
      </section>
      <section className="section--padded-t full-width">
        <InteractiveArtwork image={artworkSection.image} />
        {/* <StaticImage src='../images/panorama_optimized.jpg' alt={''} /> */}
      </section>
      <section className="section--padded-y bg-color-lightgrey">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 text-center mb-4 mx-auto">
              <h1 className="heading mb-3">{featuresSection.title}</h1>
              <p>{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row justify-content-center min-height-img">
            <FeaturesGrid list={featuresSection.features} col={2} />
          </div>
        </div>
      </section>

      <JoinUs />
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "kepala-sekolah" }) {
      id
      heroSection {
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }

        title
      }
      imageSliderSection {
        imgList {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }
      }
      artworkSection {
        image {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
            extension
            publicURL
          }
          description
        }
      }
    }
  }
`
