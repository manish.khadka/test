import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
// import ExploreTabs from 'components/ExploreTabs'
import { EmbedVideo } from '@ap-websites/shared-ui'
import FeaturesGrid from 'components/FeaturesGrid'
import HeroSection from 'components/HeroSection'
import { DynamicImage } from '@ap-websites/shared-ui'
import { GatsbyImage, StaticImage } from 'gatsby-plugin-image'
import { Field, Form, Formik } from 'formik'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section className="section--padded-y position-relative full-width">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6">
              <StaticImage
                src="../images/jelajahilmu-logo.png"
                alt={''}
                placeholder="blurred"
              />
              <div className=" default ">
                <p
                  dangerouslySetInnerHTML={{
                    __html: heroSection.description,
                  }}
                />
              </div>
              <div className="error_message_wrapper">
                <div className="error_message alert alert-danger">
                  Nama pengguna atau kata sandi Anda tidak valid. Silakan coba
                  lagi atau klik "Lupa Kata Sandi Anda?" di bawah
                </div>
                <a
                  className="text-primary"
                  id="forgot-password"
                  href="/forgot-password"
                >
                  Lupa Kata Sandi Anda?
                </a>
              </div>
              <div className="success_message_wrapper">
                <div className="success_message alert alert-success">
                  Login berhasil. Harap tunggu sementara Anda dialihkan
                </div>
              </div>
              <iframe
                src="https://app.jelajahilmu.com/indonesia-login"
                width="100%"
                height="510px"
                frameBorder="0"
                scrolling="no"
                title="indonesia-login"
              ></iframe>
            </div>
            <div className="col-md-6">
              <DynamicImage imageData={heroSection.img} alt={''} />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "login" }) {
      id
      heroSection {
        description
        img {
          childImageSharp {
            gatsbyImageData(placeholder: TRACED_SVG)
          }
          extension
          publicURL
        }
      }
    }
  }
`
