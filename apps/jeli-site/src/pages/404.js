import { Link, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'
import Layout from 'components/Layout'
import Seo from 'components/Seo'

const NotFoundPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  return (
    <Layout location={location} title={siteTitle} isFullWidth>
      <Seo title="Home" />
      <section class="section pt-5 section--bg-light">
        <div class="container text-center pb-lg-5">
          <div class="pt-md-5 pt-2 pb-4 mx-auto">
            <h1 class="heading mb-1 mt-4 h2">
              Ups! Halaman yang Anda cari tidak ada.
            </h1>
          </div>
          <p class="pb-4">
            Sepertinya Anda tersesat.
            <Link to="/" class="color-green mx-1">
              Klik disini
            </Link>
            untuk membuka halaman muka.
          </p>
          <figure class="mt-5">
            <StaticImage
              src="../images/not-found.png"
              class="img-fluid"
              width="560"
              placeholder="tracedSVG"
            />
          </figure>
        </div>
      </section>
    </Layout>
  )
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
