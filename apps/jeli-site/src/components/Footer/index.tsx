import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
// import brandIcon from 'images/mst-nepal.svg'
/* eslint-disable-next-line */
export interface FooterProps {}

export function Footer(props: FooterProps) {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          contact {
            email
            numbers
          }
          menuLinks {
            link
            name
          }
        }
      }
    }
  `)
  const menuItems = data.site.siteMetadata.menuLinks
  const contact = data.site.siteMetadata.contact
  return (
    <footer className="section-footer section-footer--light pt-4 pb-2">
      <div className="container">
        <div className="pb-2 row justify-content-center">
          <div className="col-lg-4 col-md-6 col-sm-6 pt-4">
            <div className="widget_text">
              <div className="textwidget custom-html-widget">
                <figure>
                  <StaticImage
                    src={'../../images/jelajahilmu-logo.png'}
                    alt={'logo'}
                    width={125}
                    placeholder="tracedSVG"
                  />
                </figure>
              </div>
            </div>
          </div>
          <div className="col-lg col-sm-6 pt-4">
            <div className="footer__ul">
              <div className="menu-footer-about-menu-container">
                <ul id="menu-footer-about-menu" className="menu">
                  {menuItems.map((result, index) => (
                    <li
                      key={index}
                      id="menu-item-1476"
                      className="right-triangle-pointer menu-item menu-item-type-post_type menu-item-object-page menu-item-1476"
                    >
                      <Link to={result.link}> {result.name}</Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>

          <div className="col-lg col-sm-6 pt-4">
            <div className="widget_text footer__ul">
              <h6 className="heading text-uppercase bold">Kontak</h6>
              <div className="textwidget custom-html-widget">
                <ul className="pl-0">
                  <li>
                    <a className="pl-0" href={`mailto:${contact.email}`}>
                      {contact.email}
                    </a>
                  </li>
                  {/* {contact.numbers?.map((result, index) => (
                    <li className="mb-1" key={index}>
                      <a className="pl-0" href={`tel:${result}`}>
                        {result}
                      </a>
                    </li>
                  ))} */}
                </ul>
              </div>
            </div>
          </div>
          <div className="col-lg col-sm-6 pt-4"></div>
        </div>
        <div className="footer-bottom mt-3 mb-3 d-flex">
          <div className="d-flex align-items-center">
            <small className="d-block mt-1">
              © Jelajah Ilmu {new Date().getFullYear()}
            </small>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
