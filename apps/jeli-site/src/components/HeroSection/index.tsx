import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
import { DynamicImage } from '@ap-websites/shared-ui'
/* eslint-disable-next-line */
export interface HeroSectionProps {
  isTwoColumn: boolean
  title: string
  description: string
  img: object
}

export function HeroSection(props: HeroSectionProps) {
  return (
    <div className="container">
      <div className="row justify-content-center align-items-center">
        {!props.isTwoColumn ? (
          <div className="col-lg-10 col-12">
            <h1
              className="heading mb-5 text-center h1 default"
              dangerouslySetInnerHTML={{
                __html: props.title,
              }}
            />
            <div className="w-75 mx-auto mb-3">
              <DynamicImage imageData={props.img} alt={''} />
            </div>
            <div className="default">
              <p
                className="text-center"
                dangerouslySetInnerHTML={{
                  __html: props.description,
                }}
              />
            </div>
          </div>
        ) : (
          <>
            <div className="col-md-6">
              <h1
                className="heading mb-5 h1 default"
                dangerouslySetInnerHTML={{
                  __html: props.title,
                }}
              />
              <div className=" default ">
                <p
                  dangerouslySetInnerHTML={{
                    __html: props.description,
                  }}
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className=" mx-auto">
                <DynamicImage imageData={props.img} alt={''} />
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

export default HeroSection
