import React from 'react'
import { DynamicImage } from '@ap-websites/shared-ui'

/* eslint-disable-next-line */
export interface FeaturesGridProps {
  list: Array<{
    title: string
    description: string
    img: string
    isComingSoon: boolean
  }>
  col?: number
}

export function FeaturesGrid(props: FeaturesGridProps) {
  let columns
  switch (props.col) {
    case 1:
      columns = 'col-md-10'
      break
    case 2:
      columns = 'col-md-6 col-lg-5 col-sm-10 mb-lg-5 mb-3'
      break
    case 3:
      columns = 'col-xl-4 col-lg-5 col-md-6 col-sm-12'
      break
    default:
      columns = 'col-xl-4 col-lg-5 col-md-6 col-sm-12'
      break
  }
  return (
    <>
      {props.list.map((result, index) => (
        <div className={`${columns} mb-3`} key={result.title}>
          <div className="p-3">
            {result.img && (
              <figure className="text-left mb-3">
                <DynamicImage
                  imageData={result.img}
                  className="w-md-100 mb-3"
                  imageStyle={{ height: '130px' }}
                />
              </figure>
            )}
            <article>
              <h5 className="heading mb-2">{result.title}</h5>
              <p className="mb-3">{result.description}</p>
            </article>
          </div>
        </div>
      ))}
    </>
  )
}

export default FeaturesGrid
