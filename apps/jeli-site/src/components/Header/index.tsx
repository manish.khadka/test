import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import logo from '../../images/jelajahilmu-logo.png'
import { StaticImage } from 'gatsby-plugin-image'

const Header = ({ title }) => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          menuLinks {
            link
            name
          }
        }
      }
    }
  `)
  const menuItems = data.site.siteMetadata.menuLinks
  function handleClick() {
    document.getElementById('topnav').classList.toggle('sidebar-nav--show')
    document
      .getElementById('topnav_hamburger')
      .classList.toggle('hamburger--cross')
  }
  return (
    <>
      {/* <header className="navbar-wrapper main_navbar navbar-main navbar navbar-light navbar-expand fixed-top justify-content-between mx-auto px-1 p-0"> */}
      <header className="navbar-wrapper navbar navbar-expand navbar-light bg-white main_navbar fixed-top justify-content-between mx-auto px-1 p-0">
        <div className="container ml-20">
          <i className="navbar-brand ml-3">
            <Link to="/">
              <StaticImage
                src={'../../images/jelajahilmu-logo.png'}
                alt={'logo'}
                className="img-fluid custom-logo"
                style={{ width: '125px' }}
                placeholder="tracedSVG"
              />
            </Link>
          </i>
          <div className="d-flex align-items-center mr-auto">
            <ul className="navbar-nav mt-1">
              {menuItems.map((result) => (
                <li
                  className={`nav-item menu-item menu-item-type-custom menu-item-object-custom ${
                    result.linkList ? 'always-show' : ''
                  }`}
                  key={result.name}
                >
                  <Link
                    className="nav-link"
                    target=""
                    to={result.link}
                    activeClassName={'active'}
                  >
                    {result.name}
                  </Link>
                </li>
              ))}
              <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                <a className="nav-link" href="https://help.jellajahilmu.com">
                  Help
                </a>
              </li>
            </ul>
          </div>
          <div className="d-flex align-items-center">
            <div className="nav__btn pl-2">
              <Link
                target=""
                to="/login"
                className="btn btn-primary menu-item menu-item-type-post_type menu-item-object-page menu-item-1770"
              >
                Masuk
              </Link>
            </div>
          </div>
        </div>
      </header>
      <header className="navbar main_navbar mobile-nav--wrapper navbar-light bg-white sticky-top">
        <i className="navbar-brand ml-3">
          <Link to="/">
            <StaticImage
              src={'../../images/jelajahilmu-logo.png'}
              alt={'logo'}
              className="img-fluid"
              style={{ width: '80px' }}
            />
          </Link>
        </i>

        <div className="d-flex align-items-center">
          <div
            className="hamburger pr-2"
            id="topnav_hamburger"
            onClick={() => handleClick()}
          >
            <span className="line"></span>
            <span className="line"></span>
            <span className="line"></span>
          </div>
        </div>

        <div className="mobile-nav mt-2 pt-3 mx-auto shadow" id="topnav">
          <div className="container-fluid">
            <div className="row mobile-nav__menu-ul px-4 pt-1">
              <ul className="navbar-nav mt-1">
                {menuItems.map((result) => (
                  <li
                    className="nav-item menu-item menu-item-type-custom menu-item-object-custom"
                    key={result.name}
                  >
                    <Link className="nav-link" to={result.link}>
                      {result.name}
                    </Link>
                  </li>
                ))}
                <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                  <Link
                    className="nav-link active"
                    to="/"
                    activeClassName="active"
                  >
                    Help
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}
export default Header
