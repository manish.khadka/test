import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'
// import brandIcon from 'images/mst-nepal.svg'
/* eslint-disable-next-line */
export interface JoinUsProps {
  background?: 'white' | 'lightgrey'
}

export function JoinUs(props: JoinUsProps) {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          joinUs {
            title
            ctaText
            ctaLink
          }
        }
      }
    }
  `)
  const joinUs = data.site.siteMetadata.joinUs
  return (
    <section
      className={`section--padded acf-block ${
        props.background !== 'lightgrey' ? 'bg-white' : 'bg-color-lightgrey'
      } full-width`}
    >
      <div className="container">
        <div className="row">
          <div className="col-md-6 mx-auto pb-2">
            <div className="text-center">
              <h1 className="heading">{joinUs.title}</h1>
            </div>
            <div className="nav__btn pl-3 mt-5 text-center">
              <Link
                className="btn btn-primary btn-primary--shadow btn-rounded color-white"
                to="/hubungi-kami"
                target="_blank"
                rel="noreferrer"
              >
                {joinUs.ctaText}
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default JoinUs
