module.exports = {
  siteMetadata: {
    title: `Jelajahilmu`,
    author: {
      name: `Jelajahilmu`,
      summary: `Your second Teacher :)`,
    },
    description: `Jelajahi Ilmu Pengetahuan Kapan Pun, Di Mana Pun`,
    siteUrl: `https://mysecondteacher.com.np/`,
    apiBaseUrl: `${process.env.GATSBY_API_URL}`,
    social: {
      twitter: `MySecondTeache1`,
      facebook: `https://www.facebook.com/mySecondTeacherNepal/`,
      instagram: `https://www.instagram.com/mysecondteacher_nepal/`,
      youtube: `https://www.youtube.com/c/MySecondTeacherNepal`,
    },
    joinUs: {
      title: `Bergabunglah dengan Jelajah Ilmu Hari Ini`,
      ctaText: `Hubungi Kami`,
      ctaLink: `/hubungi-kami`,
    },
    contact: {
      email: 'info@jelajahilmu.com',
      numbers: ['+977-9801010144', '+977-9801010144'],
    },
    menuLinks: [
      {
        name: 'Tentang Jelajah Ilmu',
        link: '/tentang',
      },
      {
        name: 'Siswa',
        link: '/siswa',
      },
      {
        name: 'Guru',
        link: '/guru',
      },
      {
        name: 'Kepala Sekolah',
        link: '/kepala-sekolah',
      },
      {
        name: 'Orang Tua',
        link: '/orang-tua',
      },
      {
        name: 'Hubungi Kami',
        link: '/hubungi-kami',
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-resolve-src',
      options: {
        srcPath: `${__dirname}/src`,
      },
    },
    'gatsby-plugin-sass',

    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        svgo: false,
        ref: true,
      },
    },
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: false, // Activates purging in npm run develop
        purgeOnly: ['/style.scss'], // applies purging only on the bootstrap css file
      },
    }, // must be after other CSS plugins
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/data`,
        name: `data`,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    // `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        // The option defaults to true
        checkSupportedExtensions: false,
      },
    },
    {
      resolve: 'gatsby-transformer-yaml-full',
      options: {
        plugins: [
          {
            resolve: 'gatsby-yaml-full-markdown',
            options: {
              /* gatsby-yaml-full-markdown options here */
            },
          },
        ],
      },
    },
    // 'gatsby-transformer-yaml',
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,

    // {
    //   resolve: `gatsby-plugin-sharp`,
    //   options: {
    //     defaults: {
    //       formats: [`auto`, `webp`],
    //       placeholder: `dominantColor`,
    //       quality: 50,
    //       breakpoints: [750, 1080, 1366, 1920],
    //       backgroundColor: `transparent`,
    //       tracedSVGOptions: {},
    //       blurredOptions: {},
    //       jpgOptions: {},
    //       pngOptions: {},
    //       webpOptions: {},
    //       avifOptions: {},
    //     },
    // stripMetadata: true,
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Jelajahilmu`,
        short_name: `Jeli`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon-indonesia.svg`,
      },
    },
  ],
}
