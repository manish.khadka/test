#!/usr/bin/env bash

set -euo pipefail

# requirements: following AWS_CREDENTIALS be set
#   AWS_SECRET_ACCESS_KEY
#   AWS_ACCESS_KEY_ID
#   AWS_REGION

fetch_env() {
  aws ssm get-parameter --with-decryption --name "/ci/${CI_COMMIT_BRANCH}/shared_env" \
    --query "Parameter.Value" --region "$AWS_DEFAULT_REGION" | jq -r | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]"
}

set_credentials() {
  creds=$(aws sts assume-role --role-arn "$DEPLOYER_ROLE_ARN" --role-session-name temporary_creds)
  export AWS_ACCESS_KEY_ID="$(echo $creds | jq -r ".Credentials.AccessKeyId")"
  export AWS_SECRET_ACCESS_KEY="$(echo $creds | jq -r ".Credentials.SecretAccessKey")"
  export AWS_SESSION_TOKEN="$(echo $creds | jq -r ".Credentials.SessionToken")"
}

export $(fetch_env | xargs) && set_credentials
{
  echo "AWS_ACCESS_KEY_ID_=$AWS_ACCESS_KEY_ID"
  echo "AWS_SECRET_ACCESS_KEY_=$AWS_SECRET_ACCESS_KEY"
  echo "AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN"
} >build.env

# install dependencies
rm -rf node_modules/
npm install --prefer-offline --no-audit --no-fund

npx nx build --prod mst-help-site

