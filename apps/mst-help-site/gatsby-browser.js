/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
// custom typefaces
import '@ap-websites/typeface-proxima-nova-soft'
// normalize CSS across browsers
import './src/normalize.css'
// custom CSS styles
import './src/style.scss'

// JS dependencies
import 'bootstrap/js/src/collapse'
import 'bootstrap/js/src/modal'
import '@justinribeiro/lite-youtube'
