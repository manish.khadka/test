import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { Footer } from '@ap-websites/shared-ui'
import SearchBar from 'components/SearchBar'

const Sidenav = ({ isSearchpage }) => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          title
        }
      }
      allWpHelpContentCategory(sort: { fields: id, order: ASC }) {
        nodes {
          name
          id
          slug
        }
      }
    }
  `)
  const posts = data.allWpHelpContentCategory.nodes
  const siteTitle = data.site.siteMetadata.title

  function handleClick() {
    document.getElementById('sidebar').classList.toggle('mobile-sidebar')
    document.getElementById('show-sidebar').classList.toggle('active')
    document.getElementById('hamburger').classList.toggle('hamburger--cross')
  }

  return (
    <>
      <aside className="col-lg-2 d-lg-block sidebar" id="sidebar">
        <div className="sidebar-sticky">
          {!isSearchpage && <SearchBar />}
          <ul className="nav flex-column">
            <li className="mb-0">
              <Link
                className="color-white py-2 px-3"
                to="/"
                itemProp="url"
                activeClassName="active"
              >
                Help Center
              </Link>
            </li>
            <li className="nav-item no-link">
              <div className="text-white py-2 px-3">Table of Content</div>
            </li>
            <ul className="nav flex-column">
              {posts.map((result) => {
                return (
                  <li className="mb-0" key={result.name}>
                    <Link
                      className="color-white"
                      to={`/${result.slug}`}
                      itemProp="url"
                      activeClassName="active"
                      partiallyActive={true}
                    >
                      {result.name}
                    </Link>
                  </li>
                )
              })}
            </ul>
          </ul>
          <Footer />
        </div>
      </aside>
      <button
        aria-label="Side Nav Button"
        id="show-sidebar"
        className="show-sidebar btn btn-sm"
        onClick={() => handleClick()}
      >
        <div className="hamburger" id="hamburger">
          <span className="line"></span>
          <span className="line"></span>
          <span className="line"></span>
        </div>
      </button>
    </>
  )
}
export default Sidenav
