// import React from 'react'
import EmbedVideo from 'components/EmbedVideo'
import { DynamicImage } from '@ap-websites/shared-ui'
import './index.scss'

/* eslint-disable-next-line */
export interface IndexProps {
  title: string
  id: string
  embedVideoId: string
  featuredImage?: object
}

export function Index(props: IndexProps) {
  return (
    <>
      <div
        className="video-thumnail"
        data-bs-toggle="modal"
        data-bs-target={`#${props.id}`}
      >
        {props.featuredImage ? (
          <DynamicImage imageData={props.featuredImage} alt={props.title} />
        ) : (
          <picture>
            <source
              id="webpPlaceholder"
              type="image/webp"
              srcSet={`https://i.ytimg.com/vi_webp/${props.embedVideoId}/hqdefault.webp`}
            />
            <source
              id="jpegPlaceholder"
              type="image/jpeg"
              srcSet={`https://i.ytimg.com/vi/${props.embedVideoId}/hqdefault.jpg`}
            />
            <img
              id="fallbackPlaceholder"
              referrerPolicy="origin"
              loading="lazy"
              src={`https://i.ytimg.com/vi/${props.embedVideoId}/hqdefault.jpg`}
              aria-label="Play: Video"
              alt="Play: Video"
            />
          </picture>
        )}
      </div>
      <p className="mt-1 mb-3" id="group_2">
        {props.title}
      </p>
      <div
        className="modal fade"
        id={props.id}
        tabIndex={-1}
        aria-labelledby={`${props.id}Label`}
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered text-center">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="btn-close close"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true" className="p-1">
                  ×
                </span>
              </button>
              {props.featuredImage ? (
                <DynamicImage
                  imageData={props.featuredImage}
                  alt={props.title}
                />
              ) : (
                <EmbedVideo
                  title={props.title}
                  embedVideoId={props.embedVideoId}
                  isFullWidth={true}
                />
              )}
              <p className="mt-1 mb-3" id="group_4">
                {props.title}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Index
