/* eslint-disable-next-line */
export interface EmbedVideoProps {
  title: string
  embedVideoId: string
  isFullWidth: boolean
}

export function EmbedVideo(props: EmbedVideoProps) {
  return (
    <div
      className="yt-facade-wrapper embedVideo-iframe px-3 mb-3"
      style={
        props.isFullWidth
          ? { width: '100%', height: 'auto' }
          : { maxWidth: '500px', height: 'auto' }
      }
    >
      <lite-youtube
        videoid={props.embedVideoId}
        videotitle={props.title}
        params="controls=1&enablejsapi=1&rel=0"
      ></lite-youtube>
    </div>
  )
}

export default EmbedVideo
