import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'

const Header = ({ title }) => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          menuLinks {
            link
            name
          }
        }
      }
    }
  `)
  const menuItems = data.site.siteMetadata.menuLinks
  function handleClick() {
    document.getElementById('topnav').classList.toggle('sidebar-nav--show')
    document
      .getElementById('topnav_hamburger')
      .classList.toggle('hamburger--cross')
  }
  return (
    <>
      <header className="main_navbar navbar-main navbar navbar-light navbar-expand sticky-top justify-content-between mx-auto px-1 p-0">
        <i className="navbar-brand position-absolute ml-3">
          <a href="https://mysecondteacher.com/">
            <StaticImage
              src="../../images/mst_logo_2-1.png"
              alt="logo"
              width={80}
              aspectRatio={1.36}
              placeholder="tracedSVG"
            />
          </a>
        </i>
        <div className="container ml-20">
          <div className="d-flex align-items-center">
            <ul className="navbar-nav mt-1">
              {menuItems.map((result) => (
                <li
                  className="nav-item menu-item menu-item-type-custom menu-item-object-custom"
                  key={result.name}
                >
                  <a className="nav-link" target="" href={result.link}>
                    {result.name}
                  </a>
                </li>
              ))}
              <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                <Link
                  className="nav-link active"
                  to="/"
                  activeClassName="active"
                >
                  Help
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </header>
      <header className="navbar main_navbar mobile-nav--wrapper navbar-light bg-white sticky-top">
        <i className="navbar-brand ml-3">
          <a href="https://mysecondteacher.com/">
            <StaticImage
              src="../../images/mst_logo_2-1.png"
              alt="logo"
              width={80}
              aspectRatio={1.36}
              placeholder="tracedSVG"
            />
          </a>
        </i>

        <div className="d-flex align-items-center">
          <div
            className="hamburger pr-2"
            id="topnav_hamburger"
            onClick={() => handleClick()}
          >
            <span className="line"></span>
            <span className="line"></span>
            <span className="line"></span>
          </div>
        </div>

        <div className="mobile-nav mt-2 pt-3 mx-auto shadow" id="topnav">
          <div className="container-fluid">
            <div className="row mobile-nav__menu-ul px-4 pt-1">
              <ul className="navbar-nav mt-1">
                {menuItems.map((result) => (
                  <li
                    className="nav-item menu-item menu-item-type-custom menu-item-object-custom"
                    key={result.name}
                  >
                    <a className="nav-link" target="" href={result.link}>
                      {result.name}
                    </a>
                  </li>
                ))}
                <li className="nav-item menu-item menu-item-type-custom menu-item-object-custom">
                  <Link
                    className="nav-link active"
                    to="/"
                    activeClassName="active"
                  >
                    Help
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}
export default Header
