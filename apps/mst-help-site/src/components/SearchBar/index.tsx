import React, { useState } from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { useFlexSearch } from 'react-use-flexsearch'
import { Formik, Form, Field } from 'formik'

const SearchBar = () => {
  const data = useStaticQuery(graphql`
    {
      localSearchPages {
        index
        store
      }
    }
  `)

  const index = data.localSearchPages.index
  const store = data.localSearchPages.store
  const options = {
    tokenize: 'forward',
    suggestions: true,
  }
  const [query, setQuery] = useState(null)
  const [inputValue, setInputValue] = useState('')
  const results = useFlexSearch(query, index, store, options)
  const handleChange = (e) => {
    setInputValue(e.currentTarget.value)
    setQuery(e.currentTarget.value)
  }
  const handleSubmit = (e) => {
    const element = document.querySelector('#view-all-results')
    if (element instanceof HTMLElement) {
      element.click()
    }
  }

  return (
    <div className="px-2 my-3">
      <Formik initialValues={{ query: '' }} onSubmit={handleSubmit}>
        <Form className="position-relative">
          <div className="input-group mb-3">
            <Field
              className="form-control dark"
              name="query"
              value={inputValue}
              onChange={handleChange}
              placeholder="Search help.."
              label="Side Nav Search"
              id="side-search"
              type="search"
            />
            <div className="input-group-append">
              <button
                className="btn btn-primary py-1 px-2 border-0 text-white"
                type="submit"
                style={{ backgroundColor: '#6e6884' }}
              >
                <div
                  style={{
                    WebkitTransform: 'rotate(315deg)',
                    OTransform: 'rotate(315deg)',
                    transform: 'rotate(315deg)',
                  }}
                >
                  &#9906;
                </div>
              </button>
            </div>
          </div>
          <div
            className={`position-absolute bg-white rounded shadow fadein ${
              results.length > 0 ? 'd-block' : 'd-none'
            }`}
            style={{ zIndex: 999 }}
          >
            <ul
              className="nav"
              style={{ maxHeight: '200px', overflow: 'auto' }}
            >
              {results.map((result) => (
                <li key={result.id} className="border-bottom">
                  <Link
                    to={'/' + result.category + '/' + result.slug}
                    style={{ color: '#39C9A7' }}
                  >
                    {result.title}
                  </Link>
                </li>
              ))}
            </ul>
            <li>
              <Link
                to="/search"
                state={{ searchQuery: query }}
                id="view-all-results"
              >
                View all results
              </Link>
            </li>
          </div>
        </Form>
      </Formik>
    </div>
  )
}

export default SearchBar
