import React from 'react'
import Sidenav from 'components/SideNav'
import Header from 'components/Header'

const Layout = ({ location, title, children, showNav }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  const isSearchpage = location.pathname.includes('/search')

  return (
    <>
      <Header title={title} />
      <div className="container-fluid">
        <div className="row">
          {showNav === true && <Sidenav isSearchpage={isSearchpage} />}
          <main
            className="col-md-9 ml-20 ml-5 col-lg-9 pt-3 px-4 mt-4"
            data-is-root-path={isRootPath}
          >
            <main>{children}</main>
          </main>
        </div>
      </div>
    </>
  )
}

export default Layout
