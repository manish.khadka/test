import React from 'react'
import { Link, graphql } from 'gatsby'
import Card from 'components/Card'
import Videomodal from 'components/VideoModal'
import AccordionPanel from 'components/AccordionPanel'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import { uglifyString } from '@ap-websites/utils'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heading = data.wpHelpSitePage.helpHomepage.heading
  const description = data.wpHelpSitePage.helpHomepage.description
  const subHeader = data.wpHelpSitePage.helpHomepage.subHeader
  const cards = data.wpHelpSitePage.helpHomepage.cards
  const featuredVideos = data.featuredVideosWp.nodes
  const faqTitle = data.wpHelpSitePage.helpHomepage.faqTitle
  const featuredQuestions = data.featuredQuestionsWp.nodes

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title="Home" />
      <h2 className="heading mb-3 bold">{heading}</h2>
      <h3 className="h4">{subHeader}</h3>
      {description && (
        <div
          dangerouslySetInnerHTML={{
            __html: description,
          }}
        />
      )}

      <div className="row mb-4">
        {cards.map((result) => (
          <div className="col-lg-6" key={result.title}>
            <Card
              heading={result.title}
              list={result.list}
              background={result.backgroundColor}
              icon={result.cardIcon.localFile}
            />
          </div>
        ))}
      </div>
      <div className="row mb-4">
        {featuredVideos.map((result) => (
          <div
            className="col-lg-4 col-md-6 col-sm-4 col-6"
            key={uglifyString(result.title)}
          >
            <Videomodal
              embedVideoId={result.helpContent.youtubeVideoId}
              id={uglifyString(result.title)}
              title={result.title}
              featuredImage={result.featuredImage}
            />
          </div>
        ))}
      </div>
      <h4>{faqTitle}</h4>
      <div className="accordion panel-group" id="accordion">
        {featuredQuestions.map((result) => (
          <AccordionPanel
            key={uglifyString(result.title)}
            id={uglifyString(result.title)}
            title={result.title}
            content={result.content}
            embedVideoId={result.helpContent.youtubeVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    wpHelpSitePage(title: { eq: "Homepage" }) {
      helpHomepage {
        description
        fieldGroupName
        heading
        subHeader
        cards {
          backgroundColor
          fieldGroupName
          title
          list {
            fieldGroupName
            listItem
          }
          cardIcon {
            id
            localFile {
              childImageSharp {
                gatsbyImageData
              }
              publicURL
            }
          }
        }
      }
    }
    featuredQuestionsWp: allWpHelpSiteContent(
      filter: { helpContent: { featuredPost: { eq: true } } }
      sort: { fields: date, order: ASC }
    ) {
      nodes {
        id
        title
        helpContent {
          youtubeVideoId
        }
        content
      }
    }
    featuredVideosWp: allWpHelpSiteContent(
      filter: { helpContent: { featuredVideo: { eq: true } } }
      sort: { fields: modified, order: ASC }
    ) {
      nodes {
        id
        title
        helpContent {
          youtubeVideoId
        }
      }
    }
  }
`
