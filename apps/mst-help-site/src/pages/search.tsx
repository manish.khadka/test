import React, { useState } from 'react'
import { Link, graphql } from 'gatsby'
import { useFlexSearch } from 'react-use-flexsearch'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import { Formik, Form, Field } from 'formik'
import AccordionPanel from 'components/AccordionPanel'
import { uglifyString } from '@ap-websites/utils'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const index = data.localSearchPages.index
  const store = data.localSearchPages.store
  //   const options = {
  //     tokenize: "forward",
  //     suggestions: true,
  //   }
  const initalSearch = location.state ? location.state.searchQuery : ''
  const [query, setQuery] = useState(initalSearch)
  const [inputValue, setInputValue] = useState(initalSearch)
  const results = useFlexSearch(query, index, store)
  const handleChange = (e) => {
    setInputValue(e.currentTarget.value)
    setQuery(e.currentTarget.value)
  }
  const handleSubmit = (e) => {
    const element = document.querySelector('#search')
    if (element instanceof HTMLInputElement) {
      setInputValue(element.value)
      setQuery(element.value)
    }
  }
  return (
    <Layout location={location} title={siteTitle} showNav={true}>
      <Seo title="Search" />
      <Formik
        initialValues={{
          query: location.state ? location.state.searchQuery : '',
        }}
        onSubmit={handleSubmit}
      >
        <Form>
          <div className="input-group  input-group-lg mb-4">
            <Field
              className="form-control form-control-lg"
              name="query"
              value={inputValue}
              onChange={handleChange}
              placeholder="Search help.."
              label="Search"
              id="search"
              autoFocus={true}
              type="search"
            />
            <div className="input-group-append">
              <button className="btn btn-primary" type="submit">
                <div
                  style={{
                    WebkitTransform: 'rotate(315deg)',
                    OTransform: 'rotate(315deg)',
                    transform: 'rotate(315deg)',
                  }}
                >
                  &#9906;
                </div>
              </button>
            </div>
          </div>
        </Form>
      </Formik>
      <h1 className="mb-4">Search Results for {query}:</h1>
      {results.length < 1 && <p className="my-5">No results found</p>}
      <div className="accordion panel-group" id="accordion">
        {results.map((result) => (
          <React.Fragment key={result.id}>
            <Link
              className="badge badge-primary p-1 px-2 my-2 rounded-pill"
              to={
                '/' +
                result.category.replace(/\/|\s+/g, '-').toLowerCase() +
                '/'
              }
            >
              {result.category}
            </Link>
            <AccordionPanel
              id={uglifyString(result.title)}
              title={result.title}
              content={result.body}
              embedVideoId={result.embedVideoId}
            />
          </React.Fragment>
        ))}
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    localSearchPages {
      index
      store
    }
  }
`
