import React from 'react'
import { graphql } from 'gatsby'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import AccordionPanel from 'components/AccordionPanel'
import { uglifyString } from '@ap-websites/utils'

const BlogCollectionTemplate = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const pageTitle = data.wpHelpContentCategory.name
  const posts = data.allWpHelpSiteContent.nodes

  return (
    <Layout location={location} title={siteTitle} showNav>
      <Seo title={pageTitle} description={`FAQ regarding ${pageTitle}`} />
      <header>
        <h1 className="heading" itemProp="headline">
          {pageTitle}
        </h1>
      </header>
      <div className="accordion panel-group" id="accordion">
        {posts.map((result) => (
          <AccordionPanel
            key={result.id}
            id={uglifyString(result.title)}
            title={result.title}
            content={result.content}
            embedVideoId={result.helpContent.youtubeVideoId}
          />
        ))}
      </div>
    </Layout>
  )
}

export default BlogCollectionTemplate

export const pageQuery = graphql`
  query BlogCollectionBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    wpHelpContentCategory(slug: { regex: $slug }) {
      id
      name
    }
    allWpHelpSiteContent(
      sort: { fields: date, order: ASC }
      filter: {
        helpContentCategories: {
          nodes: { elemMatch: { slug: { regex: $slug } } }
        }
      }
    ) {
      nodes {
        id
        title
        helpContentCategories {
          nodes {
            name
          }
        }
        slug
        helpContent {
          youtubeVideoId
        }
        content
      }
    }
  }
`
