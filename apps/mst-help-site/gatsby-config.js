module.exports = {
  trailingSlash: 'never',
  siteMetadata: {
    title: `MST Help Desk`,
    author: {
      name: `mySecondTeacher`,
      summary: `Your second Teacher :)`,
    },
    description: `Help Desk for mySecondTeacher`,
    siteUrl: `https://help.mysecondteacher.com/`,
    social: {
      twitter: `MySecondTeache1`,
    },
    menuLinks: [
      {
        name: 'About',
        link: 'https://mysecondteacher.com/about',
      },
      {
        name: 'Student',
        link: 'https://mysecondteacher.com/students',
      },
      {
        name: 'Teachers',
        link: 'https://mysecondteacher.com/teachers',
      },
      {
        name: 'School Leaders',
        link: 'https://mysecondteacher.com/school-leaders',
      },
      {
        name: 'Parents',
        link: 'https://mysecondteacher.com/parents',
      },
      {
        name: 'News',
        link: 'https://mysecondteacher.com/news',
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-resolve-src',
      options: {
        srcPath: `${__dirname}/src`,
      },
    },
    'gatsby-plugin-sass',
    // {
    //   resolve: 'gatsby-plugin-svgr',
    //   options: {
    //     svgo: false,
    //     ref: true,
    //   },
    // },
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: false, // Activates purging in npm run develop
        purgeOnly: ['/src/style.scss'], // applies purging only on the bootstrap css file
      },
    }, // must be after other CSS plugins
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        url:
          // allows a fallback url if WPGRAPHQL_URL is not set in the env, this may be a local or remote WP instance.
          process.env.NX_CMS_WPGRAPHQL_URL,
        schema: {
          //Prefixes all WP Types with "Wp" so "Post and allPost" become "WpPost and allWpPost".
          typePrefix: `Wp`,
        },
        develop: {
          //caches media files outside of Gatsby's default cache an thus allows them to persist through a cache reset.
          hardCacheMediaFiles: true,
        },
        html: {
          useGatsbyImage: true,
        },
        auth: {
          htaccess: {
            username: process.env.NX_CMS_HTACCESS_USER,
            password: process.env.NX_CMS_HTACCESS_PASSWORD,
          },
        },
        type: {
          Post: {
            limit:
              process.env.NODE_ENV === `development`
                ? // Lets just pull 50 posts in development to make it easy on ourselves (aka. faster).
                  50
                : // and we don't actually need more than 5000 in production for this particular site
                  5000,
          },
        },
      },
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        // A unique name for the search index. This should be descriptive of
        // what the index contains. This is required.
        name: 'pages',

        // Set the search engine to create the index. This is required.
        // The following engines are supported: flexsearch, lunr
        engine: 'flexsearch',

        // Provide options to the engine. This is optional and only recommended
        // for advanced users.
        //
        // Note: Only the flexsearch engine supports options.
        engineOptions: 'speed',
        // tokenize: "forward",
        // suggest: true,
        // depth: 3,
        // limit: 5,

        // GraphQL query used to fetch all data for the search index. This is
        // required.
        query: `
          {
            allWpHelpSiteContent(filter: {content: {ne: ""}}) {   
              nodes {
                id
                content
                excerpt
                slug
                helpContent {
                  youtubeVideoId
                }
                helpContentCategories {
                  nodes {
                    name
                    slug
                  }
                }
                title
              }
            }
          }
        `,

        // Field used as the reference value for each document.
        // Default: 'id'.
        ref: 'id',

        // List of keys to index. The values of the keys are taken from the
        // normalizer function below.
        // Default: all fields
        index: ['title', 'body', 'category'],

        // List of keys to store and make available in your UI. The values of
        // the keys are taken from the normalizer function below.
        // Default: all fields
        store: [
          'id',
          'slug',
          'title',
          'excerpt',
          'body',
          'embedVideoId',
          'category',
        ],

        // Function used to map the result from the GraphQL query. This should
        // return an array of items to index in the form of flat objects
        // containing properties to index. The objects must contain the `ref`
        // field above (default: 'id'). This is required.
        normalizer: ({ data }) =>
          data.allWpHelpSiteContent.nodes.map((node) => ({
            id: node.id,
            title: node.title,
            category: node.helpContentCategories.nodes[0].slug,
            slug: node.slug,
            body: node.content,
            embedVideoId: node.helpContent.youtubeVideoId,
            excerpt: node.excerpt,
          })),
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        // The option defaults to true
        // To suppress svg warning
        checkSupportedExtensions: false,
      },
    },
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,
    // {
    //   resolve: `gatsby-plugin-sharp`,
    //   options: {
    //     defaults: {
    //       formats: [`auto`, `webp`],
    //       placeholder: `dominantColor`,
    //       quality: 50,
    //       breakpoints: [750, 1080, 1366, 1920],
    //       backgroundColor: `transparent`,
    //       tracedSVGOptions: {},
    //       blurredOptions: {},
    //       jpgOptions: {},
    //       pngOptions: {},
    //       webpOptions: {},
    //       avifOptions: {},
    //     },
    //   },
    // },
    // TODO: Optional configs
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ['G-W02RYTB10C'],
        // gtagConfig: {
        //   optimize_id: 'OPT_CONTAINER_ID',
        //   anonymize_ip: true,
        //   cookie_expires: 0,
        // },
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
          //   // Setting this parameter is also optional
          //   respectDNT: true,
          //   // Avoids sending pageview hits from custom paths
          //   exclude: ["/preview/**", "/do-not-track/me/too/"],
          //   // Defaults to https://www.googletagmanager.com
          //   origin: "YOUR_SELF_HOSTED_ORIGIN",
        },
      },
    },
    // TODO: unnecessary can remove
    // {
    //   resolve: `gatsby-plugin-netlify`,
    //   options: {
    //     headers: {
    //       // No cache for html and page-data.json, app-data.json
    //       // `public, max-age=0, must-revalidate` can be replaced with `no-cache`
    //       '/*.html': ['cache-control: public, max-age=0, must-revalidate'],
    //       '/page-data/*': ['cache-control: public, max-age=0, must-revalidate'],
    //       // invalidate robots for dev site `:ap-sites` refers to placeholder
    //       // i.e. any subdomain in innovatetech.io to avoid the site showing up in search results
    //       'https://:ap-sites.innovatetech.io/*': ['X-Robots-Tag: noindex'],
    //     }, // option to add more headers. `Link` headers are transformed by the below criteria
    //     // allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
    //     mergeSecurityHeaders: true, // boolean to turn off the default security headers
    //     mergeCachingHeaders: true, // boolean to turn off the default caching headers
    //     // transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
    //     generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `MST Help Site`,
        short_name: `MST Help`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#6E4BEC`,
        display: `minimal-ui`,
        icon: `static/favicon-32x32.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
